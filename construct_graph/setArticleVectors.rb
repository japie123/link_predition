def setArticleVector(article)
  return if File.exists?("#{$PRE}articles_vector/"+article.id.to_s)
  f_tmp=File.new("#{$PRE}tmp_article_file","w")
  aa = article.title + article.content
  page = Nokogiri::HTML.parse aa
  article_remove = page.text
  article_remove.gsub! /[\\nt\/【】來源womany’]/," "
  article_remove.gsub! /[【.]/,""
  article_remove.gsub! /[；，。、「」()：『？』！《》'"'-〉]/,""
  f_tmp.puts article_remove
  f_tmp.close
  a = `#{$PRE}stanford-segmenter-2013-11-12/segment.sh ctb tmp_article_file UTF-8 0`
  
  # tf calculation and write out
  grams = a.split
  tf = {}
  vector = {}
  vector["unknown"] = 0
  grams.each do |gram|
    tf[gram] ||= 0
    tf[gram] += 1
  end
  tf_sort = tf.sort_by {|k,v| -v}
  maxfreg = tf_sort.first[1]
  print "tf start:"
  grams.each_with_index do |gram,index|
    if index%100==0 then print "." end
    if index%2000==0 then print "\n",index," " end
    next if not $idf.has_key? gram
    #   vector["unknown"] += 1
    #   next
    # end
    f_td = tf[gram]
    tf[gram] = 0.5 + 0.5*f_td/maxfreg
    # v = tf*idf
    # puts "gram:",gram
    # puts $idf[gram]
    vector[gram] = tf[gram]*$idf[gram]
  end
  print "tf end...\n"
  file = File.new("#{$PRE}articles_vector/"+article.id.to_s,"w")
  file.puts vector.to_yaml
  file.close
end

def setArticelVector_tf(article)
  return if File.exists?("#{$PRE}articles_vector_tf/"+article.id.to_s)
  f_tmp=File.new("#{$PRE}tmp_article_file","w")
  aa = article.title + article.content
  page = Nokogiri::HTML.parse aa
  article_remove = page.text
  article_remove.gsub! /[\\nt\/【】來源womany’]/," "
  article_remove.gsub! /[【.]/,""
  article_remove.gsub! /[；，。、「」()：『？』！《》'"'-〉]/,""
  f_tmp.puts article_remove
  f_tmp.close
  a = `#{$PRE}stanford-segmenter-2013-11-12/segment.sh ctb tmp_article_file UTF-8 0`
  
  # tf calculation and write out
  grams = a.split
  tf = {}
  vector = {}
  vector["unknown"] = 0
  grams.each do |gram|
    tf[gram] ||= 0
    tf[gram] += 1
  end
  tf_sort = tf.sort_by {|k,v| -v}
  maxfreg = tf_sort.first[1]
  print "tf start:"
  grams.each_with_index do |gram,index|
    if index%100==0 then print "." end
    if index%2000==0 then print "\n",index," " end
    # if not $idf.has_key? gram
    #   vector["unknown"] += 1
    # end
    f_td = tf[gram]
    tf[gram] = 0.5 + 0.5*f_td/maxfreg
    # v = tf*idf
    # puts "gram:",gram
    # puts $idf[gram]
    # vector[gram] = tf[gram]*$idf[gram]
  end
  print "tf end...\n"
  file = File.new("#{$PRE}articles_vector_tf/"+article.id.to_s,"w")
  file.puts tf.to_yaml
  file.close
end