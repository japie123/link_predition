require 'mysql2'
require 'json'
require 'yaml'
require 'nokogiri'
# require 'ckip_client'
STEP = 1

client = Mysql2::Client.new(:host => "localhost", :username => "root", :password => "1235813", :database => "link_prediction")
query_len = "select count(id) from articles"
articles_length = client.query(query_len, :as => :array).first.first
puts 'articles_length',articles_length
dict = []
idf = {}

# off = 0
(0..articles_length).step(STEP) do |off|
  printf "\noff: %d", off
  sql = "select title,content from articles order by id limit #{STEP} offset #{off}"
  # sql = "select title,content from articles order by id "
  articles = client.query(sql, :as => :hash)

  tmp_file = "log/tmp_3.txt"
  f = File.open(tmp_file, 'w')

  articles.each_with_index do |article, index|
    if (index)%100 == 0 then puts; print index; end
    printf  '.'
    aa = article['title'] + article['content']
    page = Nokogiri::HTML.parse aa
    article_remove = page.text
    article_remove.gsub! /[\\nt\/【】來源womany’]/," "
    article_remove.gsub! /[【.]/,""
    article_remove.gsub! /[；，。、「」()：『？』！《》'"'-〉]/,""
    f.puts article_remove
  end
  f.close
  print "get text start..\n"
  text = `./stanford-segmenter-2013-11-12/segment.sh ctb #{tmp_file} UTF-8 0 `
  print "text gsub done\n"
  puts Time.now

  # write out cache
  file = File.open("text2_"+off.to_s,"w")
  file.puts text
  file.close

  # add to dict set.
  puts '===='
  print "generate dict...\n"
  d = []
  delete = []
  text.each_line do |line|
    wordArray = line.split
    printf "."
    wordArray.each do |w|
      if delete.include? w
        print "!"
        next 
      end
      delete << w
      idf[w] ||=0
      idf[w] += 1
      d << w      
    end
  end
  dict = dict | d
  puts "",Time.now
end

File.open('womany_dictionary_5.yml', 'w') {|f| f.puts dict.to_yaml}
File.open('idf_original_5.yml', 'w') {|f| f.puts idf.to_yaml}

idf.each do |k,v|
  if v==0 then puts k end
  idf[k] = Math.log( articles_length / v )
end
File.open('idf_5.yml', 'w') {|f| f.puts idf.to_yaml}

