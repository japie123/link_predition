puts 'constructgraph.rb'
DICTIONARY = "womany_dictionary_5.yml"
$dict = YAML::load_file DICTIONARY
# $idf = YAML::load_file 'idf_5.yml'
load 'construct_graph/setArticleVectors.rb'

def constructgraph(cas)
  # case1: from womany database
  # case2: from ptt
  # case3: from simulation
  userSet = []
  articleSet = []
  if File.exists?($LINK_ARTICLE_USER_NAME+"lalala")
    puts "loading full article-user cache ...:"
    puts $LINK_ARTICLE_USER_NAME
    arr = YAML.load_file($LINK_ARTICLE_USER_NAME)
    userSet = arr[0]
    articleSet = arr[1]
  # elsif File.exists?($LINK_ARTICLE_USER_NAME_LIMIT)
  #   puts "loading limit article-user cache ...:"
  #   puts $LINK_ARTICLE_USER_NAME_LIMIT
  #   arr = YAML.load_file($LINK_ARTICLE_USER_NAME_LIMIT)
  #   userSet = arr[0]
  #   articleSet = arr[1]
  else
    case cas
    when "data"
      # link to database and query 'hits'
      puts "select all tag relation..."
      client = Mysql2::Client.new(:host => "localhost", :username => "root", :password => "1235813", :database => "link_prediction")

      puts "select hits ..."    
      # f = ""
      # if $LIMITDATA
      #   f = File.new($LINK_ARTICLE_USER_NAME_LIMIT,"w")
      #   puts 'LIMITDATA!'
      #   result = client.query("select * from hits where action_name = 'article' and user_id <> 'nil' and param_id <> 'nil' limit 20000;")
      # else
        result = client.query("select * from hits where action_name = 'article' and user_id <> 'nil' and param_id <> 'nil';")
      # end
      article_read = []
      hit_hash = {}
      puts "result.size", result.size
      result.each_with_index do |x, index|
        if index%50000 == 0 then printf "\n%d ",index end
        if index%500 == 0 then printf "." end

        # record ids of articles hit 
        article_read << x["param_id"]

        # record user read article ids
        if hit_hash.has_key?(x['user_id'])
          hit_hash[x['user_id']].push(x['param_id'])
        else
          hit_hash[x['user_id']] = [x['param_id']]
        end
      end

      # remove the duplicate elements
      article_read.uniq!

      print "article_read.size: ",article_read.size

      puts "\nTime.now:"
      puts Time.now
      $f.puts "Time.now:"
      $f.puts Time.now

      puts "select all tag relation..."      
      result = client.query("select * from taggings where taggable_type = 'Article' and taggable_id <> 'nil' and tag_id <> 'nil' and context = '#{$TAG_CONTEXT}';")
      print "\nresult.size: ",result.size
      hash = {}
      result.each do |x|
        if hash.has_key?(x['taggable_id'])
          hash[x['taggable_id']].push(x['tag_id'])
        else
          hash[x['taggable_id']] = [x['tag_id']]
        end
      end
      print "\nhash.size: ",hash.size
      puts "set all articles..."
      hash.each_key do |key|
        next if not article_read.include? key
        a = Article.new(key);
        for j in 0..hash[key].count-1
          a.concepts[hash[key][j]] = 1.0;
        end
        article = client.query("select * from articles where id = #{key}").first
        if not article.nil?
          a.title = article['title']
          a.content = article['content']
          articleSet.push(a)

          # construct vectors of each article 
          # setArticelVector(a)
          setArticelVector_tf(a)

        end
      end
      print "\narticleSet.szie: ",articleSet.size
      puts "\nTime.now:"
      $f.puts "Time.now:"
      $f.puts Time.now
      puts Time.now

      puts "link article and users ..."
      # {user_id => [param_id, param_id_2, ...]}
      printf "hit_hash.size: %d",hit_hash.size
      index = 0
      hit_hash.each_key do |key|
        if index%1000 == 0 then printf "\n%d ",index end
        if index%10 == 0 then printf "*" end
        index += 1
        u = User.new(key)
        for j in 0..hit_hash[key].count-1
          aID = hit_hash[key][j]
          if not u.articleIDs.include? aID
            u.articleIDs << aID
          end
          a = articleSet.find { |x| x.id == aID }
          if not a.nil?
            articleSet.delete_if { |x| x.id == aID }
            a.users << u
            articleSet.push << a
          end
        end
        userSet.push(u)
      end
      print userSet.size

    when "ptt"
    when "random"
      if not $TESTFORMINCUT
        puts "set user..."
        for i in 0..$USERNUMBER-1
          u = User.new(i);
          userSet.push(u)
        end

        puts "set article..."
        for i in 0..$ARTICLENUMBER-1
          a = Article.new(i);
          randomTag(a)
          # print i,'-tags: ',a.tags
          # puts
          articleSet.push(a)
          # break
        end

        puts "set seeing of user and articles..."
        userSet.each_with_index do |u,index|
          if index%5000==0 then print "\n",index," " end
          if index%100==0 then print "*" end
          articleSet = randomSeeArticle(u, articleSet)
        end
      else
      # Test for min-cut
        u1 = User.new(1);
        u2 = User.new(2);
        for i in 0..9
          a = Article.new(i);
          for j in 0..$TAGNUMBER-1
            a.tags[j] = 1
          end

          if i < 5
            a.users << u1
            u1.articleIDs << i
            userSet << u1
          else
            a.users << u2
            u2.articleIDs << i
            userSet << u2
          end
          articleSet.push(a)
        end
        u2 = userSet.find {|x| x.id == 2}
        userSet.delete_if {|x| x.id == 2}
        u2.articleIDs << 1
        userSet << u2
        a = articleSet.find {|x| x.id == 1}
        articleSet.delete_if {|x| x.id == 1}
        a.users << u2
        articleSet << a


        u3 = User.new(3);
        u4 = User.new(4);
        for i in 10..19
          a = Article.new(i);
          for j in 0..$TAGNUMBER-1
            a.tags[j] = 0
          end
          
          if i < 15
            a.users << u3
            u3.articleIDs << i
            userSet << u3
          else
            a.users << u4
            u4.articleIDs << i
            userSet << u4
          end
          articleSet.push(a)
        end
        u4 = userSet.find {|x| x.id == 4}
        userSet.delete_if {|x| x.id == 4}
        u4.articleIDs << 10
        userSet << u4
        a = articleSet.find {|x| x.id == 10}
        articleSet.delete_if {|x| x.id == 10}
        a.users << u4
        articleSet << a

        u5 = User.new(5);
        u6 = User.new(6);
        for i in 20..29
          a = Article.new(i);
          for j in 0..$TAGNUMBER-1
            if j%2 == 0
              a.tags[j] = 0
            else
              a.tags[j] = 1
            end
          end

          if i < 25
            a.users << u5
            u5.articleIDs << i
            userSet << u5
          else
            a.users << u6
            u6.articleIDs << i
            userSet << u6
          end
          articleSet.push(a)
        end
        u6 = userSet.find {|x| x.id == 6}
        userSet.delete_if {|x| x.id == 6}
        u6.articleIDs << 20
        userSet << u6
        a = articleSet.find {|x| x.id == 20}
        articleSet.delete_if {|x| x.id == 20}
        a.users << u6
        articleSet << a

        u7 = User.new(7);
        u8 = User.new(8);
        for i in 30..39
          a = Article.new(i);
          for j in 0..$TAGNUMBER-1
            if j%2 == 0
              a.tags[j] = 1
            else
              a.tags[j] = 0
            end
          end
        
          if i < 35
            a.users << u7
            u7.articleIDs << i
            userSet << u7
          else
            a.users << u8
            u8.articleIDs << i
            userSet << u8
          end
          articleSet.push(a)
        end
        u8 = userSet.find {|x| x.id == 8}
        userSet.delete_if {|x| x.id == 8}
        u8.articleIDs << 30
        userSet << u8
        a = articleSet.find {|x| x.id == 30}
        articleSet.delete_if {|x| x.id == 30}
        a.users << u8
        articleSet << a

        u9 = User.new(9);
        u10 = User.new(10);
        for i in 40..49
          a = Article.new(i);
          for j in 0..$TAGNUMBER-1
            if j < $TAGNUMBER/2
              a.tags[j] = 1
            else
              a.tags[j] = 0
            end
          end
        
          if i < 45
            a.users << u9
            u9.articleIDs << i
            userSet << u9
          else
            a.users << u10
            u10.articleIDs << i
            userSet << u10
          end
          articleSet.push(a)
        end
        u10 = userSet.find {|x| x.id == 10}
        userSet.delete_if {|x| x.id == 10}
        u10.articleIDs << 40
        userSet << u10
        a = articleSet.find {|x| x.id == 40}
        articleSet.delete_if {|x| x.id == 40}
        a.users << u10
        articleSet << a
      end

      # Test
      # for i in 0..20;
      #   a = Article.new()
      #   b = Article.new()
      #   for j in 0..$TAGNUMBER
      #     a.tags[j] = 1
      #     b.tags[j] = 0
      #   end
      #   articleSet.push(a)
      #   articleSet.push(b)
      # end

      # debug
      # puts 'userSet.length:', userSet.length
      # puts 'articleSet.length:', articleSet.length
      # u = userSet.sample
      # uId = u.id
      # puts 'uId',uId
      # aId = u.articleIDs.sample
      # puts 'aId',aId
      # a = articleSet.find {|x| x.id == aId }
      # puts 'aId',a.id
      # if a.nil?
      #   puts 'a is nil!!'
      # end
      # u2 = a.users.find {|x| x.id == uId }
      # puts u2.id
      # if u2.id == uId
      #   puts 'yes!'
      # end
    else
    end
    f = File.new("userSet_"+$LINK_ARTICLE_USER_NAME,"w")
    f.puts userSet.to_yaml
    f.close
    f = File.new("articleSet_"+$LINK_ARTICLE_USER_NAME,"w")
    f.puts articleSet.to_yaml
    f.close
  end
  return userSet, articleSet
end

def randomTag(type)
  # random decide how much tag have value ...
  # with accuracy of 2 decimal points
  # reassign value to some index is not considered
  tags_number = rand($TAGNUMBER.to_i) + 1
  # printf 'tags_number: %d ',tags_number
  for j in 0..tags_number-1
    t_index = rand($TAGNUMBER.to_i) # 0~$TAGNUMBER-1
    if $TAGDISCRETE
      type.concepts[t_index] = rand(2)
    else
      type.concepts[t_index] = (rand*100).round/100.0;
    end
  end
end

# only for simulation
def randomSeeArticle(u, articleSet)
  randoms = Set.new
  loop do
    randoms << rand($ARTICLENUMBER)
    break if randoms.size >= $ARTICLE_NUMBER_OF_ONEUSER
  end
  randoms.each do |index|
    u.articleIDs << index
    a = articleSet.find { |x| x.id == index }
    articleSet.delete_if { |x| x.id == index }
    a.users << u
    articleSet << a
  end
  articleSet
end
