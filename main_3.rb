print "ruby main_3.rb\n"
require 'yaml'
require 'pry'
require 'set'

$PRE = ARGV[1]
print $PRE
load "#{ARGV[1]}configure.rb"
load "#{$PRE}node.rb"
load "#{$PRE}clustering/clustering.rb"
load "#{$PRE}methods.rb"
load "#{$PRE}gen0.rb"
load "#{$PRE}iterative_2.rb"
Dir.mkdir("#{$PRE}plots") unless File.directory?("#{$PRE}plots")
Dir.mkdir("#{$PRE}ratio") unless File.directory?("#{$PRE}ratio")
Dir.mkdir("#{$PRE}split") unless File.exist?"#{$PRE}split"
Dir.mkdir("#{$PRE}cache/#{ARGV[0]}") unless File.exist?"#{$PRE}cache/#{ARGV[0]}"
Dir.mkdir("#{$PRE}cache/#{ARGV[0]}/groups") unless File.exist?"#{$PRE}cache/#{ARGV[0]}/groups"

puts "load articleSet..."
# articleSet = YAML.load_file("#{$PRE}cache/articleSet_interest")
articleSet = YAML.load_file("#{$PRE}cache/articleSet_interest_rand100")
puts "load userSet..."
userSet = YAML.load_file("#{$PRE}cache/userSet_interest")
# userSet = userSet[400...700]
# count = Set.new
# while count.size < 100
#   print "."
#   count << rand(articleSet.size)
# end

# articleSet_c = articleSet.clone
# articleSet = []
# count.each do |c|
#   articleSet << articleSet_c[c]
# end

#  initial conceptSet: 
conceptSet = []
hash = {}
articleSet.each do |a|
  a_vector = YAML.load_file("#{$PRE}articles_vector/#{a.id}")
  a.tags['unknown'] = a_vector['unknown']
  k = a.concepts.first[0]
  hash[k] ||= []
  hash[k] << a
end
# binding.pry
conceptID= []
hash.each_with_index do |h,i|
  c = Concept.new(i+1)
  conceptID << h[0]
  c.articles.replace h[1]
  c.update_center
  conceptSet << c
end
# f = File.new("data_vector","w")
# print "\nlala"
# articleSet.each do |a|
#   conceptID.each do |cid|
#     if a.concepts.has_key?cid
#       f.print a.concepts[cid]," "
#     else
#       f.print 0," "    
#     end
#   end
#   f.print "\n"
# end
# f.close

# gen0
features= []
# Dir.mkdir("#{$PRE}cache/gen0_feature") unless File.directory?("#{$PRE}cache/gen0_feature")
if not File.exist?("#{$PRE}cache/#{ARGV[0]}/gen0_features")
# if true
  print "\nget gen0 features..."
  features, articleSet, conceptSet = gen0 conceptSet,articleSet
  
  print "\nwrite out cache features_sel and all_articles_..."
  f = File.new("#{$PRE}cache/#{ARGV[0]}/gen0_features","w")
  f.puts features.to_yaml
  f.close
  f = File.new("#{$PRE}cache/#{ARGV[0]}/gen0_articleSet","w")
  f.puts articleSet.to_yaml
  f.close
  f = File.new("#{$PRE}cache/#{ARGV[0]}/gen0_conceptSet","w")
  f.puts conceptSet.to_yaml
  f.close
else
  print "\nload f,a,c..."
  features = YAML.load_file("#{$PRE}cache/#{ARGV[0]}/gen0_features")
  print "\nload a..."
  articleSet = YAML.load_file("#{$PRE}cache/#{ARGV[0]}/gen0_articleSet")
  print "\nload c..."
  conceptSet = YAML.load_file("#{$PRE}cache/#{ARGV[0]}/gen0_conceptSet")
end

# iterative
print "\niterative, load rb..."
load "#{$PRE}decide_user_concept/decide_user_concept.rb"
load "#{$PRE}converge.rb"
load "#{$PRE}rejudge_concept/rejudge_concept.rb"
load "#{$PRE}adjust_article_tags/adjust_article_tags.rb"
load "#{$PRE}clusterPredict.rb"

carr = []
conceptSet.each do |cc|
  c = Concept.new(cc.id,cc.center)
  c.articles.replace cc.articles
  c.number_of_users =  cc.number_of_users
  carr << c
end

# fork do 
  # if features.empty?
  #   print "!!features.empty?"
  # end
  clusterPredict ARGV[0],userSet,carr,features,ARGV[0]+"_gen0"
# end
# print "\nstart  iterative..."
iterative conceptSet,features,articleSet,userSet




