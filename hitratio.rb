
def hitratio userSet_predict, userSet_right, dir, all_hit
  arr = [4,6,8,10,12,14]
  count = {}
  arr.each do |l|
    count[l] = 0
  end

  userSet_predict.each do |u_p|
    p_articles = u_p.articleIDs
    u_r = userSet_right.find { |x| x.id == u_p.id }
    if u_r.nil?
      # print "x"
      next
    end
    r_articles = u_r.articleIDs

    p_articles.each_with_index do |p_id,i|
      break if i > arr.last
      if r_articles.include?p_id
        arr.each do |level|
          if i < level
            count[level] += 1
          end
        end
      end
    end
  end
  f = File.new("#{$PRE}plots/hit_#{dir}","w")  
  print "\nhit: "
  count.each do |h|
    f.print h[1]/all_hit.to_f," "
    print "(",h[0],",",h[1]/all_hit.to_f,") "
  end
  f.close
end