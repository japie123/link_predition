load 'node.rb'
require 'pry'
require 'yaml'

# read c_split
# c_split = []
# d = Dir["c_split/*"]
# d.each_with_index do |f,index|
#   c_split[index] = File.open(f,"r")
# end
puts "load clustering..."
cluster = YAML.load_file("cache/clustering_datafalsefalsefalse0")
puts "load userSet..."
userSet = YAML.load_file("cache/user_concept_datafalsefalsefalse0")

sum = 0
countone = 0
cluster.each_with_index do |c,i|
  # if i%10 == 0 then print i," " end
  #  actial weight
  # ---------------
  #   total weight
  articles = c.articles
  len = articles.size
  total_weight = len*(len-1)/2.0
  sigma = 0

  if total_weight == 0
    countone += 1
    next
  end
  print "\nlen: ",len,"\n"
  # caculate actial weight
  actial_weight = 0
  for i in 0..len-1
    print i," "
    for j in i+1..len-1
      ai = articles[i]
      aj = articles[j]
      sum_weight = 0.0;
      (ai.users & aj.users).each do |u|
        u = userSet.find {|x| x.id == u.id}
        if u.conceptSet[c.id].nil?
          binding.pry
        end
        sum_weight += u.conceptSet[c.id] # get concept weight for this user
      end
      
      if sum_weight < 0
        print "\nsum_weight < 0!!\n"
      end
      sigma += sum_weight
      if sum_weight > 0
        actial_weight += sum_weight
      end
    end
  end
  # total weight = edges*capacity
  total_weight *= sigma
  print actial_weight,"/",total_weight,"\n"
  sum += actial_weight/total_weight
end
print "\ncluster.size: ",cluster.size
print "\ncountone: ",countone
print "\nsum: ",sum
sum /= (cluster.size-countone)
print "average  proportion: ",sum


def weight_common_user(concept, ai, aj)
  # sum the weight of user on this concept
  # the less ditance of this user, the more he influent
  sum_weight = 0.0;
  (ai.users & aj.users).each do |u|
    # if $COMMON_USER_SUM
    sum_weight += u.conceptSet[concept.id] # get concept weight for this user
    # else
    #   sum_weight += -1*u.conceptSet[concept.id] # the less seen article, the more important
    # end
  end
  
  if sum_weight < 0
    print "\nsum_weight < 0!!\n"
  end
  
  return sum_weight
end

