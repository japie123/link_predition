require 'mysql2'
require 'yaml'
require 'pry'
load 'node.rb'
load 'construct_graph/setArticleVectors.rb'
load 'pd_eq7.rb'
load 'configure.rb'
load 'decide_user_concept/decide_user_concept.rb'
load 'accuracy.rb'

puts Time.now
print "\n$ruby clusterArticles.rb v# cache/iter_feature/v#/[concept features userSet] dir\n"
version = ARGV[0]
concache = ARGV[1]
feature = ARGV[2]
print "\nloading userSet..."
# userSet = YAML.load_file("cache/iter_feature/#{version}/#{ARGV[3]}")
dir = ARGV[4]
new_concepts = []
new_aids=[]
if File.exist?"cache/new_concepts_#{version}_#{ARGV[3]}"
  print "load new_concepts new_concepts_#{version}_#{ARGV[3]}"
  new_concepts = YAML.load_file("cache/new_concepts_#{version}_#{ARGV[3]}")
else
  # obj User with articleIDs.
  print "load conceptSet"
  conceptSet = YAML.load_file("cache/iter_feature/#{version}/#{concache}")

  # compute concept centers??
  print "conceptSet.size: ",conceptSet.size
  conceptSet_ori = conceptSet.clone
  conceptSet_add = []
  conceptSet_ori.each do |c|
    # c_new = Cluster.new(c.articles[0])
    # c.articles.each
    # binding.pry
    next if not c.center.nil?
    print "!nil "
    centerTags = {}
    c.articles.each do |a,index|
      a.concepts.each do |t|
        centerTags[t[0]] = centerTags.has_key?(t[0]) ? centerTags[t[0]] + t[1] :t[1]
      end
    end
    centerTags.keys.each do |k|
      centerTags[k] = centerTags[k] / articles.length.to_f
    end
    c.center = Article.new(0,centerTags)
    conceptSet_add << c
  end
  print "\nconceptSet_add.size: ",conceptSet_add.size
  print "\nconceptSet.size: ",conceptSet.size
  if not conceptSet_add.empty?
    print "\nconceptSet = conceptSet_add"
    conceptSet = conceptSet_add
  end
  # binding.pry

  print "\nclassify new articles according to two groups of keywords"
  print "\nloading keyword..."
  keywords = YAML.load_file("cache/iter_feature/#{version}/#{feature}")
  # classify new articles according to two groups of keywords
  print "\nloading new_articleSet..."
  new_articleSet = YAML.load_file("cache/new_articleSet")
  new_articleSet.each do |a|
    # print "\nset vector..."
    setArticleVector(a)
    # find the nearest concept
    # print "\nfind the nearest concept"
    a_vector = YAML.load_file("articles_vector/#{a.id}")
    keywords.each do |k|
      if a_vector.has_key? k
        # print a_vector[k]," "
        a.tags[k] = a_vector[k]
      else
        a.tags[k] = 0.0
      end
    end
    min_dis = Float::INFINITY
    c_belong = nil
    conceptSet.each do |c|
      dis = a.distance_to c.center
      if dis < min_dis
        min_dis = dis
        c_belong = c.clone
      end
    end
    if c_belong.nil?
      print "\n [Error!!] c_belong.nil!"
      binding.pry
    end
    c_belong.addArticle(a)
    c_belong.update_center
    
    c_del = conceptSet.find {|x| x.id==c_belong.id}
    conceptSet.delete(c_del)
    conceptSet << c_belong

  end
  print "\nconcepts.size: ",conceptSet.size
  puts "\n",Time.now

  file=File.new("cache/new_concepts_#{version}_#{ARGV[3]}","w")
  file.puts conceptSet.to_yaml
  file.close
end

# print "\nload new articles..."
# new_articleSet = YAML.load_file("cache/new_articleSet")
# new_articleSet.each do |a|
#   new_aids << a.id
# end
# binding.pry
# print "\nloading prob..."
# prob = YAML.load_file("bigram_prob_inv")

# new_concepts.each do |c|
#   c.articles.each do |a|
#     a.concepts[c.id] = 1
#   end
# end

# print "\nprdict..."
# userSet_predict = predict(new_concepts, userSet, prob,new_aids)
# # binding.pry
# userSet_right = YAML.load_file("cache/new_reading")
# Dir.mkdir("plots/#{dir}") unless File.exist?"plots/#{dir}"

# accuracy(userSet_predict, userSet_right, dir, 100)
