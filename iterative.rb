require 'yaml'
require 'mysql2'
require 'nokogiri'
require 'pry'
require 'set'

print "ruby iterative version cache/concept cache/features cache/articleSet"

load 'configure.rb'
load 'node.rb'
load 'clustering/clustering.rb'
load 'decide_user_concept/decide_user_concept.rb'
load 'converge.rb'
load 'rejudge_concept/rejudge_concept.rb'
load 'adjust_article_tags/adjust_article_tags.rb'
load 'methods.rb'

# load
version = ARGV[0]
Dir.mkdir("m2_c_split/#{version}") unless File.directory?("m2_c_split/#{version}")
print "\nload conceptSet"
conceptSet = YAML.load_file("cache/#{ARGV[1]}")
print "\nload features"
features = YAML.load_file("cache/#{ARGV[2]}")
print "\nload articleSet"
articleSet = YAML.load_file("cache/#{ARGV[3]}")
print "\nload userSet"
userSet = YAML.load_file("cache/m2_userSet_datafalsefalsefalse0")
# ====================================
# iteration
# use features_sel to clustering
if true
  print "\nreduce concpet"
  articleSet_copy = articleSet.clone
  len = articleSet.size
  articleSet = []
  articleSet_c = Set.new
  while articleSet_c.size < 100
    print "."
    articleSet_c << rand(len)
  end
  all_id = []
  articleSet_c.each do |a|
    print a," "
    all_id << articleSet_copy[a].id
    articleSet << articleSet_copy[a]
  end
  print "\nclean concept"
  conceptSet_c = conceptSet.clone
  conceptSet_c.each do |c|
    c_all = c.articles.clone
    c_all.each do |a|
      if not all_id.include?a.id
        c.articles.delete(a)
      end
    end
    if c.articles.size==0
      conceptSet.delete(c)
    end
  end
else
  print "\noverall database"
end
iteration = 0
print "\n total concept size: ",conceptSet.size
print "\n total features size: ",features.size
print "\n total articleSet size: ",articleSet.size
while true
  puts "\n",Time.now
  Dir.mkdir("m2_c_split/#{version}/iter#{iteration}") unless File.directory?("m2_c_split/#{version}/iter#{iteration}")
  Dir.mkdir("cache/#{ARGV[0]}") unless File.directory?("cache/#{ARGV[0]}")
  Dir.mkdir("cache/#{ARGV[0]}/iter#{iteration}") unless File.directory?("cache/#{ARGV[0]}/iter#{iteration}")
  Dir.mkdir("cache/iter_feature_2/#{ARGV[0]}") unless File.directory?("cache/iter_feature_2/#{ARGV[0]}")
  
  print "\n\n",iteration," iteration: "

  print "\ndecide_user_concept..."
  userSet = decide_user_concept(conceptSet, userSet, iteration)
  print "\nrejudge_concept..."  
  c_split = []
  c_merge, c_split = rejudge_concept(conceptSet,userSet,iteration.to_s)
  # binding.pry
  if converge(c_merge, c_split)
    break;
  end
  for i in 0...c_split.size
    print "."
    next if File.exist? "m2_c_split/#{version}/iter#{iteration}/#{i}"
    f = File.new("m2_c_split/#{version}/iter#{iteration}/#{i}","w")
    f.puts c_split[i].to_yaml
    f.close
  end

  # generate keywords
  # regenerate feature every time
  $REGENERATE = false
  if $REGENERATE
    articleSet.each do |a|
      a.tags = {}
    end
  end
  if articleSet.empty? or conceptSet.empty?
    binding.pry
  end 
  articleSet,conceptSet,features_sel = adjust_article_tags(c_merge, c_split, articleSet, conceptSet, iteration)
  if articleSet.empty? or conceptSet.empty? or features_sel.empty?
    binding.pry
  end 
  # cluster
  puts Time.now
  print "write out cache features_sel_iter#{iteration} and all_articles_iter#{iteration}..."
  f = File.new("cache/#{ARGV[0]}/iter#{iteration}/features_sel_iter#{iteration}","w")
  f.puts features_sel.to_yaml
  f.close
  f = File.new("cache/#{ARGV[0]}/iter#{iteration}/all_articles_iter#{iteration}","w")
  f.puts articleSet.to_yaml
  f.close
  f = File.new("cache/#{ARGV[0]}/iter#{iteration}/conceptSet_iter#{iteration}","w")
  f.puts conceptSet.to_yaml
  f.close
  print "write out cache userSet_iter#{iteration}..."
  f = File.new("cache/#{ARGV[0]}/iter#{iteration}/userSet_iter#{iteration}","w")
  f.puts userSet.to_yaml
  f.close

  iteration +=1
end