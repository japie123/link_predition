puts "methods.rb"
SELECTED_RELATIVE_COUNT = 8
def dis ai_t,center
  ai_keys = ai_t.keys
  c_keys = center.keys
  all_keys = ai_keys | c_keys
  Math.sqrt( all_keys.inject(0){ |sum, key| sum + (has_k?(ai_t,key) - has_k?(center,key))**2 } )
end

def has_k?(tags,key)
  return tags.has_key?(key) ? tags[key] : 0
end

def distance conceptSet_new,conceptSet_ori
  print "\nconceptSet_new.size: ",conceptSet_new.size
  print "\nconceptSet_ori.size: ",conceptSet_ori.size
  conceptSet_left = conceptSet_ori.clone
  sum_intersect = 0.0
  total = 0.0
  conceptSet_new.each do |c_n|
    print ">"
    del_c = nil
    # a_n = []
    # c_n.articles.each do |a|
    #   a_n << a.id
    # end
    sum = 0.0
    c_n.articles.each do |a|
      sum += a.concepts[c_n.id]
    end
    # if not K-means, need change!
    total += sum
    max_intersect = 0.0

    # find the closetst concept
    # binding.pry
    conceptSet_left.each do |c_o|
      # a_o = []
      # c_o.articles.each do |a|
      #   a_o << a.id
      # end
      sum_o = 0.0

      c_o.articles.each do |a|
        if a.concepts.has_key? c_o.id
          sum_o += a.concepts[c_o.id]
        end
      end      
      # intersect = compare(a_n,a_o)
      intersect = [sum,sum_o].min
      if intersect > max_intersect
        max_intersect = intersect
        print  " max_intersect: ",max_intersect
        del_c = c_o
      end
    end
    if del_c.nil?
      # binding.pry
    end
    sum_intersect += max_intersect
    conceptSet_left.delete(del_c)
    if conceptSet_left.empty?
      print "conceptSet_left.empty"
    end
  end
  print "\ntotal: ",total
  print " return sum_intersect: ",sum_intersect
  # sum_diff/total
  sum_intersect
end

def compare a_n, a_o
  intersect = a_n & a_o
  # if intersect.size > 0
  #   print " intersect.size: ",intersect.size
  # end
  # (a_n | a_o).size, (a_n - intersect).size + (a_o - intersect).size
  intersect.size
end

def get_feature c_s
  
  print "c_s[2].articles.size: ",c_s[2].articles.size
  if c_s[2].articles.nil?
    print "c_s[2].articles.nil"
  end
  groupOne = c_s[2].articles
  groupTwo = c_s[3].articles
  is_gOne = {}
  is_gTwo = {}
  if groupOne.size == 0 or groupTwo.size == 0
    binding.pry
    return nil
  end
  
  $DEBUG = true
  i = c_s[0]

  # feature selection - for calculating n-gram
  print "\ngroupOne.size: ",groupOne.size
  groupOne.each do |a|
    ngram_arr = ngram(a.id)
    ngram_arr.each do |item|
      is_gOne[item] ||= 0 
      is_gOne[item] += 1
    end
  end

  print "\nwrite out groupTwo...\n"
  print "\ngroupTwo.size: ",groupTwo.size,"\n"
  groupTwo.each_with_index do |a,ind|
    ngram_arr = ngram(a.id)
    ngram_arr.each do |item|
      is_gTwo[item] ||= 0 
      is_gTwo[item] += 1
    end
  end
  
  # Calculate chi-square statistic of each ngram
  print "\nCalculating chi-square"
  chi_square = get_chi2 groupOne,groupTwo, is_gOne, is_gTwo

  puts " Sorting chi-square"
  print "chi_square.size: ",chi_square.size,"\n"
  sorted_chi = chi_square.sort_by{|k,v|-1*v}  
  current_chi = sorted_chi[0][1].to_f  
  current_chi_grams = []
  final_chi = []
  sorted_chi.each_with_index do |entry,ind|
    if ind%500 == 0 then print ind," " end
    if float_equal(current_chi, entry[1].to_f)
      current_chi_grams << entry[0]
    else
      # encountered a gram with different current gram.
      final_chi = write_to_final_chi final_chi, current_chi_grams, current_chi
      # Set current chi & renew the grams.
      current_chi = entry[1].to_f
      current_chi_grams = [entry[0]]
    end
  end
  
  # Process the last batch of grams
  print "\nwrite_to_final_chi: from ",final_chi.size
  final_chi = write_to_final_chi final_chi, current_chi_grams, current_chi
  # binding.pry
  print "\nExtracting the most important words..."
  print "\nfinal_chi.length:",final_chi.length
  sorted_chi_sel = final_chi[0...SELECTED_RELATIVE_COUNT] 
  # + final_chi[-SELECTED_RELATIVE_COUNT..-1]
  
  puts "\nWriting Feature_selected.txt..."
  print  "sorted_chi_sel.size: ",sorted_chi_sel.size
  print "return sorted_chi_sel\n"
  return sorted_chi_sel
end


def ngram a_id
  if not File.exist?("#{$PRE}article_ngram_arr/#{a_id}")
    a_vector = YAML.load_file("#{$PRE}articles_vector/#{a_id}")
    if a_vector.nil? then puts "a_vector.nil!" end
    ngram_arr = []
    a_vector.each do |hash|
      next if hash[0] == 'unknown'
      print "."
      ngram_arr << hash[0]
    end
    f = File.new("#{$PRE}article_ngram_arr/#{a_id}","w")
    f.puts ngram_arr.to_yaml
    f.close
    return ngram_arr
  else
    return ngram_arr = YAML.load_file("#{$PRE}article_ngram_arr/#{a_id}")
  end
end

def write_to_final_chi final_chi, current_chi_grams, current_chi
  if current_chi_grams.size > 10000
    print "\nwrite_to_final_chi current_chi_grams.size > 10000"
  end
  # print "\ncurrent_chi_grams.size: ",current_chi_grams.size
  current_chi_grams.sort! {|a, b| a.size <=> b.size}
  grams_hash = {}
  # remain the longest feature
  current_chi_grams.each do |gram|
    # user ffirst word to be hash
    grams_hash[gram[0]] ||= []
    grams_hash[gram[0]] << gram
  end

  # tack last gram each array in each hash
  grams = []
  grams_hash.values.each do |arr|
    grams.sort! {|a, b| a.size <=> b.size}
    grams << arr.last
  end
  # (0...current_chi_grams.size-1).each_with_index do |i,index|
  #   # if index%100 == 0 
  #   #   printf "%d ",index 
  #   # end
  #   reserved = true
  #   gram = current_chi_grams[i]
  
  #   (i+1...current_chi_grams.size).each do |j|
  #     if not current_chi_grams[j][gram].nil?
  #       reserved = false
  #       break
  #     end
  #   end
  #   grams << gram if reserved
  # end
  # # print "\ngrams.size: ",grams.size
  grams.each do |g|
    final_chi << [g, current_chi]
  end
  
  final_chi
end

def get_chi2 groupOne, groupTwo, is_gOne, is_gTwo
  # A = #(t,c)  | C = #(~t,c)
  # ------------+--------------
  # B = #(t,~c) | D = #(~t, ~c)

  #            N*(A*D - C*B)^2
  # chi2 = -------------------------
  #         (A+C)*(B+D)*(A+B)*(C+D)

  print "\n = get_chi2"
  chi_square = {}
  terms = is_gOne.keys + is_gTwo.keys
  terms.each_with_index do |gram,ind|
    if ind%20000==0 then print ind," " end
    a = is_gOne[gram].to_f
    b = is_gTwo[gram].to_f
    c = groupOne.length - a
    d = groupTwo.length - b

    # if a + b < 0.1
    #   puts "Skipping word #{gram}"
    #   next
    # end
    # sign = (a*d-b*c) >= 0 ? 1 : -1
    n = groupOne.length + groupTwo.length
    # chi2 = sign * n * ((a*d-b*c) ** 2) / ((a+c)*(b+d)*(a+b)*(c+d))
    numerator = ((a*d-b*c) ** 2)
    denominator = ((a+c)*(b+d)*(a+b)*(c+d))
    next if denominator == 0.0
    chi2 = n *  numerator / denominator.to_f
    chi_square[gram] = chi2
  end
  print "return chi_square"
  return chi_square
end


def float_equal(a, b)
  (a-b).abs < 0.00000000001
end
