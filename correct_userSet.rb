load 'node.rb'
require 'yaml'

puts "loading full article-user cache ...:"
puts "userSet_articleSet_cache_new_datafalsefalsefalse0"
arr = YAML.load_file("cache/userSet_articleSet_cache_new_datafalsefalsefalse0")
userSet = arr[0]
print "userSet.size: " ,userSet.size
new_userSet = []
exist_user = []
userSet.each_with_index do |user,index|
  if index%1000 == 0 then printf "\n%d ",index end
  if index%10 == 0 then print "-" end
  next if exist_user.include?user.id
  print "*"
  exist_user << user.id
  new_userSet << user;
end
print "after correct, userSet.size: " ,new_userSet.size

print "\nwrite out new_userSet..."
f= File.new("cache/m2_userSet_datafalsefalsefalse0","w")
f.puts new_userSet.to_yaml
f.close