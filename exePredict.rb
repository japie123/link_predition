print "ruby exePredict v0 '' iteration"  
require 'yaml'
$PRE = ARGV[1]
print "\n$PRE: ",$PRE
load "#{$PRE}node.rb"
load "#{$PRE}clusterPredict.rb"
iteration = ARGV[2]
fset = YAML.load_file("#{$PRE}cache/#{ARGV[0]}/iter#{iteration}_features")
conceptSet = YAML.load_file("#{$PRE}cache/#{ARGV[0]}/iter#{iteration}_conceptSet")
userSet = YAML.load_file("#{$PRE}cache/#{ARGV[0]}/userSet_iter#{iteration}")

clusterPredict ARGV[0],userSet,conceptSet,fset,ARGV[0]+"_iter#{iteration}" 