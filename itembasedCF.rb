
$PRE = ARGV[1]
load "#{$PRE}node.rb"
require 'yaml'
require 'set'
require 'pry'
load "#{$PRE}accuracy.rb"
load "#{$PRE}hitratio.rb"
load "#{$PRE}roc.rb"
print "\nload articleSet..."
articleSet = YAML.load_file("#{$PRE}cache/m2_articleSet_datafalsefalsefalse0")
# articleSet = articleSet[-100...-1]
# randomsize = ARGV[1].to_i
# articleSet_c = Set.new
# while articleSet_c.size < randomsize
#   print "."
#   articleSet_c << rand(articleSet.size)
# end

# all_id = []
# articleSet_c.each do |a|
#   all_id << a
# end
print "\nload userSet_right..."
userSet_right = YAML.load_file("#{$PRE}cache/new_reading")
all_hit = 0
userSet_right.each do |u|
  all_hit += u.articleIDs.size
end

print "articleSet.size: ",articleSet.size
# cos similarity of two items
sim = {}
simsum = {}
articleSet.each_with_index do |a,index|  
  if index%1000 == 0 
    puts "\n",Time.now
    print index," " 
  end
  if index%5 == 0 then print "." end 
  list = {}
  simsum[a.id] ||= 0
  articleSet.each do |alink|
    next if alink.id == a.id
    # common user
    intersect =  (a.users & alink.users).size
    if intersect > 0
      simsum[a.id] += intersect.to_f/(a.users.size*alink.users.size)
      list[alink.id] = intersect.to_f/(a.users.size*alink.users.size)
    end
  end
  # list_s = list.sort_by {|k,v| -1*v}
  sim[a.id] = list
end
puts "\n",Time.now

f=File.new("simsum","w")
f.puts simsum.to_yaml
f.close
f=File.new("sim","w")
f.puts sim.to_yaml
f.close
print "\nwrite out article similarity"
# f = File.new("cache/articleSimilarity","w")
# f.puts sim.to_yaml
# f.close
# binding.pry

# each user's item's similar items
print "\nloading userSet..."
userSet = YAML.load_file("#{$PRE}cache/m2_userSet_datafalsefalsefalse0")
userSet_predict = []
print "\npredict..\n"
userSet = userSet[0...10000]
userSet.each_with_index do |u,i|  
  if i%1000 == 0 
    puts "\n",Time.now
    print i," "
  end
  print "."
  aids = Set.new
  u.articleIDs.each_with_index do |a_id,ind|
    # next if not all_id.include? a_id
    if sim[a_id].nil?
      next
    end
    sim[a_id].each do |list|
      aids << list[0] #otherarticle      
    end
  end
  # binding.pry
  # only caculate the possible items 
  score = {}
  aids.each do |aid|
    sum = 0.0
    u.articleIDs.each do |uaid|
      if not sim[aid][uaid].nil?
        sum += sim[aid][uaid]
      end
    end
    score[aid] = sum/  simsum[aid]
  end
  # binding.pry

  sort_article = score.sort_by {|k,v| -1*v}
  u_new = User.new(u.id)
  sort_article.each do |a|
    # next if not all_id.include? a
    u_new.articleIDs << a
  end
  if u_new.articleIDs.size==0
    print "?! "
  end
  userSet_predict << u_new
end
# f= File.new("#{$PRE}cache/userSet_predict_itembasedCF_#{ARGV[0]}","w")
# f.puts userSet_predict.to_yaml
# f.close

accuracy(userSet_predict, userSet_right, "itembasedCF_#{ARGV[0]}", 100)
# hitratio(userSet_predict, userSet_right, "itembasedCF_#{ARGV[0]}", all_hit)
# roc(userSet_predict, userSet_right, "itembasedCF_#{ARGV[0]}")
# binding.pry
