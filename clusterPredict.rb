# require "mysql2"
# require "yaml"
# require "pry"
# load "node.rb"

load "#{$PRE}construct_graph/setArticleVectors.rb"
load "#{$PRE}pd_eq7.rb"
# load "configure.rb"
load "#{$PRE}decide_user_concept/decide_user_concept.rb"
load "#{$PRE}accuracy.rb"
load "#{$PRE}hitratio.rb"
load "#{$PRE}roc.rb"

def clusterPredict version,userSet,c,features,dir
conceptSet = c.clone
puts Time.now
m=2
userSet_predict = nil
if File.exist?("#{$PRE}cache/#{ARGV[0]}/user_predict_#{dir}")
  print "\nload userSet_predict..."
  userSet_predict = YAML.load_file("#{$PRE}cache/#{ARGV[0]}/user_predict_#{dir}")
else
new_aids=[]
new_conceptSet = []
if File.exist?"#{$PRE}cache/#{ARGV[0]}/new_concepts_#{dir}"
  print "load conceptSet new_concepts_#{dir}"
  new_conceptSet = YAML.load_file("#{$PRE}cache/#{ARGV[0]}/new_concepts_#{dir}")
else
  # obj User with articleIDs.
  # print "load conceptSet"
  # conceptSet = YAML.load_file("cache/iter_feature/#{version}/#{concache}")

  # compute concept centers??
  print "conceptSet.size: ",conceptSet.size
  conceptSet_ori = conceptSet.clone
  conceptSet_add = []
  conceptSet_ori.each do |c|
    # c_new = Cluster.new(c.articles[0])
    # c.articles.each
    # binding.pry
    next if not c.center.nil?
    print "!nil "
    centerTags = {}
    c.articles.each do |a,index|
      a.concepts.each do |t|
        centerTags[t[0]] = centerTags.has_key?(t[0]) ? centerTags[t[0]] + t[1] :t[1]
      end
    end
    centerTags.keys.each do |k|
      centerTags[k] = centerTags[k] / c.articles.length.to_f
    end
    c.center = centerTags
    conceptSet_add << c
  end
  print "\nconceptSet_add.size: ",conceptSet_add.size
  print "\nconceptSet.size: ",conceptSet.size
  if not conceptSet_add.empty?
    print "\nconceptSet = conceptSet_add"
    conceptSet = conceptSet_add
  end
  # binding.pry 

  print "\nclassify new articles according to two groups of keywords"
  print "\nloading keyword..."
  # classify new articles according to two groups of keywords
  print "\nloading new_articleSet..."
  new_articleSet = YAML.load_file("#{$PRE}cache/new_articleSet")
  new_articleSet.each do |a|
    # print "\nset vector..."
    setArticleVector(a)
    # find the nearest concept
    # print "\nfind the nearest concept"
    a_vector = YAML.load_file("#{$PRE}articles_vector/#{a.id}")
    features.each do |k|
      if a_vector.has_key? k
        # print a_vector[k]," "
        a.tags[k] = a_vector[k]
      # else
      #   a.tags[k] = 0.0
      end
    end
    min_dis = Float::INFINITY
    c_belong = nil
    sum_c = 0.0
    a.concepts = {}
    # binding.pry
    conceptSet.each do |cj|
      sum = 0.0
      dij = dis(a.tags,cj.center)
      conceptSet.each do |ck|
        dik = dis(a.tags,ck.center)
        sum += ( (dij/dik )**(2/(m-1)) )
      end
      if sum > 0.0
        a.concepts[cj.id] = 1/sum
      else
        a.concepts[cj.id] = 1.0
      end
      cj.articles << a
      sum_c += a.concepts[cj.id]
    end
    a.concepts.each do |h|      
      # update the cocnepts
      c_belong = conceptSet.find {|x| x.id==h[0]}
      if c_belong.nil?
        print "\n [Error!!] c_belong.nil!"
        # binding.pry
      end
      conceptSet.delete(c_belong)
      c_belong.articles.delete a
      h[1] /= sum_c
      a.concepts[h[0]] = h[1]
      c_belong.articles << a
      c_belong.update_center

      # centerTags = {}

      # c_belong.articles.each do |a|
      #   a.tags.each do |t|
      #     centerTags[t[0]] = centerTags.has_key?(t[0]) ? centerTags[t[0]] + t[1] :t[1]
      #   end
      # end

      # # average
      # centerTags.keys.each do |k|
      #   centerTags[k] = centerTags[k] / c_belong.articles.length.to_f
      # end 

      conceptSet << c_belong
    end
    
    # c_del = conceptSet.find {|x| x.id==c_belong.id}    
  end
  # binding.pry
  print "\nconcepts.size: ",conceptSet.size
  puts "\n",Time.now
  print "\nwrite out file #{$PRE}cache/#{ARGV[0]}/new_concepts_#{dir}"

  file=File.new("#{$PRE}cache/#{ARGV[0]}/new_concepts_#{dir}","w")
  file.puts conceptSet.to_yaml
  new_conceptSet = conceptSet.clone
  file.close
end
# threads = []
# threads << Thread.new do
  print "\nload new articles..."
  new_articleSet = YAML.load_file("#{$PRE}cache/new_articleSet")
  new_articleSet.each do |a|
    new_aids << a.id
  end
  # binding.pry
  new_conceptSet.each do |c|

    c.articles.each do |a|
      a.concepts[c.id] = 1
    end
  end
# end
# binding.pry
# threads << Thread.new do
  print "\nloading prob..."
  prob = YAML.load_file("#{$PRE}bigram_prob_inv")
# end

# threads.each{|t| t.join}
# binding.pry

  userSet_predict = predict(new_conceptSet, userSet, prob,new_aids)
  f = File.open("#{$PRE}cache/#{ARGV[0]}/user_predict_#{dir}","w")
  f.puts userSet_predict.to_yaml
  f.close
end
# binding.pry
userSet_right = YAML.load_file("#{$PRE}cache/new_reading")
all_hit = 0
userSet_right.each do |u|
  all_hit += u.articleIDs.size
end

accuracy(userSet_predict, userSet_right, dir, 100)
hitratio(userSet_predict, userSet_right, dir, all_hit)
roc(userSet_predict, userSet_right, dir)
end 

def dis ai_t,center
  ai_keys = ai_t.keys
  c_keys = center.keys
  all_keys = ai_keys | c_keys
  Math.sqrt( all_keys.inject(0){ |sum, key| sum + (has_k?(ai_t,key) - has_k?(center,key))**2 } )
end

def has_k?(tags,key)
  return tags.has_key?(key) ? tags[key] : 0
end