require 'pry'
require 'set'
require 'yaml'
require 'mysql2'
require 'nokogiri'

puts 'main.rb'
load 'configure.rb'
load 'node.rb'
load 'construct_graph/construct_graph.rb'
load 'clustering/clustering.rb'
load 'decide_user_concept/decide_user_concept.rb'
load 'converge.rb'
load 'rejudge_concept/rejudge_concept.rb'
load 'adjust_article_tags/adjust_article_tags.rb'
# construct initial graph: 
# 1. article combined tag
# 2. link user and article
puts "Timmmmmme"
puts Time.now
if not $SKIP
  userSet, articleSet = constructgraph("data")
  print "\narticleSet.length: ",articleSet.length
  puts
  print "userSet.length: ",userSet.length
  # binding.pry
end
if $DEBUG_more
  $f.write "userSet[0].id: "
  $f.write userSet[0].id
  $f.write userSet[0]
end
time = 0
# repeat until converge
while true do
  puts "\nTimmmmmme"
  puts Time.now
  $f.puts "\nTimmmmmme"
  $f.puts Time.now
  if $SKIP
    c_merge = []
    c_split = []
    print "loading c_split: "
    for i in 0...93
      print i," "      
      c_split[i] = YAML.load_file("c_split/#{i}")      
    end
    puts "SKIP ... \nload userSet..."
    arr = YAML.load_file($LINK_ARTICLE_USER_NAME)
    userSet = arr[0]
    puts "load conceptSet..."
    conceptSet = YAML.load_file($CLUSTERING_CACHE)
    print "\narticleSet.length: ",conceptSet.length
    print "\nuserSet.length: ",userSet.length
  else
    printf "\n === clustering start ===\n"
    $f.write "\n === clustering start ===\n"
    # cluster article to concept
    conceptSet = clustering($CLUSERTING_TYPE, articleSet)
    printf "\n === clustering end ===\n"
    $f.write "\n === clustering end ===\n"
    $f.write "main: conceptSet.length\n"
    $f.write conceptSet.length
    if $TESTFORMINCUT
      $f.write "conceptSet[0]:\n"
      conceptSet[0].articles.each do |a|
        $f.write a.id
        $f.write ", "
      end
      $f.write "\n"
    end
    # assign concept to user
    puts
    puts "Timmmmmme"
    puts Time.now
    $f.puts "Timmmmmme"
    $f.puts Time.now
    printf "\n === decide_user_concept start ===\n"
    $f.write "\n === decide_user_concept start ===\n"
    userSet = decide_user_concept(conceptSet, userSet) 
    printf "\n === decide_user_concept end ===\n"
    $f.write "\n === decide_user_concept end ===\n"
    
    puts "Timmmmmme"
    puts Time.now
    $f.puts "Timmmmmme"
    $f.puts Time.now
    # deceide if any concept need to split or merge
    printf "\n === rejudge_concept start ===\n"
    $f.write "\n === rejudge_concept start ===\n"
    c_merge, c_split = rejudge_concept(conceptSet,userSet)
    printf "\n === rejudge_concept end ===\n"
    $f.write "\n === rejudge_concept end ===\n"
    if converge(c_merge, c_split)
      break;
    end
    if time > 10
      break
    end
    time += 1
    # print "c_split.size: ",c_split.size
    # # print c_split.to_yaml
    # print c_split[0].to_yaml
    for i in 0...c_split.size
      print "."
      next if File.exist? "c_split/#{i}"
      f = File.new("c_split/#{i}","w")
      f.puts c_split[i].to_yaml
      f.close
    end
    # binding.pry
    # puts "\nwrite c_split..."
    # f.puts c_split.to_yaml
    # f.close

    # adjust article tags    
  end
  puts "Timmmmmme"
  puts Time.now
  printf "\n === adjust_article_tags start ===\n"
  $f.write "\n === adjust_article_tags start ===\n"
  articleSet = adjust_article_tags(c_merge, c_split, articleSet)
  printf "\n === rejudge_concept end ===\n"
  $f.write "\n === adjust_article_tags end ===\n"
  $f.close
end 
