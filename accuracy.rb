
# how much 
# PRcurve, average accuracy rate
def accuracy userSet_predict, userSet_right, dir, thre = 100
  print "\nuserSet_predict.size: ",userSet_predict.size
  print "\nuserSet_right.size: ",userSet_right.size

  # user not read any articles not included
  # articlesIDs of user in userSet_predict must ranked
  # Dir.mkdir("#{$PRE}plots/#{dir}") unless File.directory?("#{$PRE}plots/#{dir}")
  f = File.new("#{$PRE}plots/#{dir}.txt","w")
  f2 = File.new("#{$PRE}plots/#{dir}_2.txt","w")
  fav = File.new("#{$PRE}plots/#{dir}_avP.txt","w")
  total_hit = 0.0 
  total_sum = 0.0 
  totlavP = 0.0
  userSet_predict.each_with_index do |u_p,ind|
    if ind%100==0 then print " ",ind," " end
    u_r = userSet_right.find { |x| x.id == u_p.id }
    if u_r.nil?
      # print "x"
      next
    end
    r_articles = u_r.articleIDs
    p_articles = u_p.articleIDs
    hit = 0.0
    total_right_size = r_articles.size
    total_sum += total_right_size
    precision_set = []
    recall_set = []
    avP = 0.0
    p_articles.each_with_index do |p_a, ind|
      # break if hit >= r_articles.size
      if r_articles.include?p_a
        # update PR when hit occur
        print "!"
        hit += 1
        total_hit += 1
        precision_set << hit/(ind+1)
        recall_set << hit/total_right_size
        avP += hit/(ind+1)
      end
    end 
    if not precision_set.empty?
      print "\n",precision_set
      print "\n",recall_set      
      precision_set.each do |p|
        f.print p," "
        f2.print p," "
      end
      f.puts
      f2.puts
      recall_set.each do |r|
        f.print r," "
        f2.print r," "
      end
      f.puts
      f2.puts
      aa = avP/precision_set.size
      totlavP += aa
      fav.print aa," "
    else
      fav.print 0," "
      f.print "empty: ",r_articles.size,"\n"
    end
  end
  f.close
  f2.close
  fav.close
  f3 = File.new("#{$PRE}ratio/#{dir}_ratio.txt","w")
  f3.print "total_hit: ", total_hit
  f3.print "\ntotal_sum: ", total_sum
  f3.print "\nratio: ", total_hit/total_sum
  f3.print "\ntotal AvP", totlavP/userSet_predict.size
  f3.close
end
