def roc userSet_predict, userSet_right, dir
  # y:TPR = TP/(TP+FN)
  # x:FPR = FP/(FP+FN)
  arr = [20,40,60,80,100]
  arr.each do |k|
    f = File.new("#{$PRE}plots/roc_#{dir}_#{k}","w")
    userSet_predict.each do |u_p|
      u_r = userSet_right.find { |x| x.id == u_p.id }
      next if u_r.nil?
      r_articles = u_r.articleIDs

      if k > u_p.articleIDs.size
        k = u_p.articleIDs.size
      end
      p_articles = u_p.articleIDs[0...k]
      tp = (p_articles & r_articles).size
      fp = p_articles.size - tp
      fn = r_articles.size - tp
      tpr = tp/(tp+fn).to_f
      fpr = fp/(fp+fn).to_f
      f.print tpr," ",fpr,"\n"
    end
    f.close
  end
end