print "ruby pre_noa.rb version predict_cache dir_name rightname acc"
require 'pry'
require 'yaml'
load 'accuracy.rb'
load 'node.rb'

print "\nload predict #{ARGV[1]}"
userSet_predict = YAML.load_file("cache/#{ARGV[1]}")
print "\nload cache/new_reading#{ARGV[3]}..."
userSet_right = YAML.load_file("cache/new_reading#{ARGV[3]}")
Dir.mkdir("plots/#{ARGV[2]}") unless File.exist?"plots/#{ARGV[2]}"
acc = ARGV[3].to_i
accuracy(userSet_predict, userSet_right, ARGV[2], acc)