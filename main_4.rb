print "ruby main_4.rb\n"
require 'yaml'
require 'pry'
require 'set'

$PRE = ARGV[1]
print $PRE
load "#{ARGV[1]}configure.rb"
load "#{$PRE}node.rb"
load "#{$PRE}clustering/clustering.rb"
load "#{$PRE}methods.rb"
load "#{$PRE}gen0.rb"
load "#{$PRE}iterative_2.rb"
Dir.mkdir("#{$PRE}plots") unless File.directory?("#{$PRE}plots")
Dir.mkdir("#{$PRE}ratio") unless File.directory?("#{$PRE}ratio")
Dir.mkdir("#{$PRE}split") unless File.exist?"#{$PRE}split"
Dir.mkdir("#{$PRE}cache/#{ARGV[0]}") unless File.exist?"#{$PRE}cache/#{ARGV[0]}"
Dir.mkdir("#{$PRE}cache/#{ARGV[0]}/groups") unless File.exist?"#{$PRE}cache/#{ARGV[0]}/groups"

puts "load articleSet..."
# articleSet = YAML.load_file("#{$PRE}cache/articleSet_interest")
articleSet = YAML.load_file("#{$PRE}cache/articleSet_interest_rand100")
puts "load userSet..."
userSet = YAML.load_file("#{$PRE}cache/userSet_interest")

conceptSet = []
c = Concept.new(1)
articleSet.each do |a|
  c.articles << a
end
c.update_center
conceptSet << c


print "\niterative, load rb..."
load "#{$PRE}decide_user_concept/decide_user_concept.rb"
load "#{$PRE}converge.rb"
load "#{$PRE}rejudge_concept/rejudge_concept.rb"
load "#{$PRE}adjust_article_tags/adjust_article_tags.rb"
load "#{$PRE}clusterPredict.rb"

features = []
clusterPredict ARGV[0],userSet,conceptSet,features,ARGV[0]+"_gen0"


iterative conceptSet,features,articleSet,userSet
