puts 'rejudge_concept'
load "#{$PRE}rejudge_concept/cut_of_groups.rb"
load "#{$PRE}rejudge_concept/exclusive.rb"
load "#{$PRE}rejudge_concept/karger.rb"
load "#{$PRE}rejudge_concept/likarger.rb"
load "#{$PRE}rejudge_concept/edmonds_karp.rb"
DIST_USER_CONCEPT_THRES = 0.015 # random average 0.035~0.06
# SPLIT_THRES = 0.01
# $totalsum = 0;
# $time = 0;
def rejudge_concept(conceptSet, userSet, iter="")
  puts "\n*** decide_user_concept ***\n"

  c_merge = []
  c_split = []
  len = conceptSet.length
  # check if there are any two concepts should be merge
  for i in 0..len-1
    c1 = conceptSet[i]    
    puts "\n",Time.now
    print "\nconcept: ",c1.id," - ["
    c1.articles.each do |a|
      print a.id,", "
      # if $DEBUG then $-f.print a.id,", " end
    end
    print "]\n"
    # check if this concept should be split
    should_split, cut, groupOne, groupTwo = should_split? c1,iter
    if should_split
      # only record the id of this concept
      c_split << [c1.id, cut, groupOne, groupTwo]
    end

    # merge is left to be decided
    # for j in 0..len-1
    #   c2 = conceptSet[j]
    #   if should_merge?(c1, c2, userSet)
    #     # only record the ids of two concepts
    #     c_merge << [c1.id, c2.id]
    #   end
    # end
  end
  # print "wrte out #{$SPLITFILE}"
  # f = File.new($SPLITFILE,"w")
  # f.printf "\nc_split: \n"
  # printf "\nwrite  out c_split: #{$SPLITFILE} \n"
  # c_split.each do |c|
  #   if c[1] > 0
  #     f.puts "\n=====",c[0]
  #     f.print "groupOne.articles.size:  "
  #     f.print c[2].articles.size
  #     f.print "\ngroupTwo.articles.size:  "
  #     f.print c[3].articles.size
  #     f.print "\ncut:"
  #     f.print c[1]
  #     f.print "\n[ "
  #     c[2].articles.each do |a|
  #       f.print a.id," "
  #     end
  #     f.print "]\n ["
  #     c[3].articles.each do |a|
  #       f.print a.id," "
  #     end
  #     f.print "]\n*******"
  #   end
  # end
  # f.puts "\n cut == 0: \n"
  
  # c_split.each do |c|
  #   if c[1] == 0
  #     f.puts "\n=====",c[0]
  #     f.print "groupOne.articles.size:  "
  #     f.print c[2].articles.size
  #     f.print "\ngroupTwo.articles.size:  "
  #     f.print c[3].articles.size
  #     f.print "\ncut: "
  #     f.print c[1]
  #     f.print "\n[ "
  #     c[2].articles.each do |a|
  #       f.print a.id," "
  #     end
  #     f.print "]\n ["
  #     c[3].articles.each do |a|
  #       f.print a.id," "
  #     end
  #     f.print "]\n*******"
  #   end
  # end
  # f.close
  return c_merge, c_split
end

def should_merge?(c1, c2, userSet,iter)
  # 如果 user 與這兩個 concept 的距離差的平方平均小於
  # DIST_USER_CONCEPT_THRES，則應該合在一起。

  diffsum = 0
  userSet.each do |u|
    # if DEBUG
    #   $$-f.write "\nc1.id: "
    #   $$-f.write c1.id
    #   $$-f.write "\nu.conceptSet[c1.id]: \n"
    #   $$-f.write u.conceptSet[c1.id]
    # end
    diffsum += (u.conceptSet[c1.id]-u.conceptSet[c2.id])**2
  end
  # $-f.write "\ndiffsum: "
  # $-f.write Math.sqrt(diffsum)*10000
  $time += 1
  $totalsum += diffsum*10000
  # 簡化寫法：有空再說
  # diffsum = 0
  # puts 'diffsum2'
  # puts Math.sqrt( userSet.each { |diffsum, u| diffsum + (u.conceptSet[c1.id] - u.conceptSet[c2.id])**2 } )
  if diffsum < DIST_USER_CONCEPT_THRES
    return true
  end
  false
end

#############     #################
#############     #################
#####             ######       ######
#####             ######       ######
###############   ######       ######
###############   ###################
          #####   #################
          #####   ######
###############   ######
###############   ######

def should_split?(concept,iter)
  # if $DEBUG then $-f.write "\nshould_split?\n" end
  # if the min-cut is less than SPLIT_THRES,
  # this concept should be split.

  # construct article link graph
  # the capacity is based on the weight of common user
  # the more/less active user effect more?
  article_weight(concept);

  len = concept.articles.length
    
  articles = concept.articles
  articles.each do |a|
    a.self_link = 0
    a.other_link = 0
    a.side = 0
  end

  # write out article weight
  if not $EXCLUSIVEWEIGHT and not File.exists?("#{$PRE}article_weight/Article_Weight_#{ARGV[0]}_#{concept.id}")
    printf "\n\n === linking article start ===\n"
    # if $DEBUG then $$-f.write "\n\n === linking article start ===\n" end
    count = 0
    for i in 0..len-1
      for j in i+1..len-1
        w = weight_common_user(concept, articles[i], articles[j]) 
        if w > 0
          count +=1
          articles[i],articles[j] = link_article(w,articles[i],articles[j])
        end
      end
    end
    print "total link: ",count
    printf "\n === linking article end ===\n"

    # if $DEBUG then $$-f.write "\n === linking article end ===\n" end
    f = File.new("#{$PRE}article_weight/Article_Weight_#{ARGV[0]}_#{concept.id}","w")
    puts "articles.size",articles.size
    articles.each do |a|
      f.printf "\n%d: \n",a.id
      aa = a.link_articles
      aa.keys.each do |linked_a|
        f.print "(", linked_a.id,", ", aa[linked_a], ")\n"
      end
    end
    f.close
    f = File.new("#{$PRE}article_weight/Article_Weight_#{ARGV[0]}_#{concept.id}_yml","w")
    f.puts articles.to_yaml
    f.close
    puts "\nwrite article weight done"
  elsif $EXCLUSIVEWEIGHT #and not File.exists?("article_weight_excl_2/Article_Weight_#{ARGV[0]}_#{concept.id}_#{iter}.yml")
    print "\nlink exclusive articles: "
    count = 0
    max_w = -1*Float::INFINITY
    weight = []
    for i in 0..len-1
      print i," "
      for j in i+1..len-1
        w = weight_common_user(concept, articles[i], articles[j]) 
        if w > max_w then max_w = w end        
        weight << w
      end
    end
    for i in 0..len-1
      print i," "
      for j in i+1..len-1
        w = max_w - weight.shift
        # print w," "
        if w < 0 then print "\nw < 0!!\n" end
        articles[i],articles[j] = link_article(w,articles[i],articles[j])
      end
    end

  #   f = File.new("article_weight_excl_2/Article_Weight_#{ARGV[0]}_#{concept.id}_#{iter}.yml","w")
  #   f.puts articles.to_yaml
  #   f.close
  # elsif not $EXCLUSIVEWEIGHT
  #   puts "\nload article weight cache Article_Weight_#{ARGV[0]}_#{concept.id}_#{iter}_yml..."
  #   articles = YAML.load_file("article_weight/Article_Weight_#{ARGV[0]}_#{concept.id}_#{iter}_yml")
  #   puts "\n load cache ends"
  # elsif $EXCLUSIVEWEIGHT
  #   puts "\nload excl article weight cache Article_Weight_#{ARGV[0]}_#{concept.id}_#{iter}.yml..."
  #   articles = YAML.load_file("article_weight_excl_2/Article_Weight_#{ARGV[0]}_#{concept.id}_#{iter}.yml")
  #   puts "\n load cache ends"
  end
  groupOne = []
  groupTwo = []
  # min_cut = Float::INFINITY
  m_cut = nil
  groupOne = nil
  groupTwo = nil
  case $CUT_TYPE
  when "greedy"
    puts "\ngreedy exclusive_weight"
    # max_cut
    m_cut, groupOne, groupTwo = exclusive concept,articles
    # print articles.length
    if m_cut.nil?
      return false
    end
  when "excluKarger"
    puts "\nKarger exclusive_weight"

    m_cut, groupOne, groupTwo = Karger articles,"min"

  when "Edmonds_Karp"
    puts "\nEdmonds_Karp"
    # min_cut
    m_cut, groupOne, groupTwo = Edmonds_Karp articles
    
  when "liKarger"
    print "\nliKarger!"
    m_cut, groupOne, groupTwo = liKarger articles
    f = File.new("#{$PRE}cache/#{ARGV[0]}/groups/iter#{iter}_#{concept.id}_t#{i}","w")
    if m_cut > 0
      groupSet = []
      groupSet << groupOne
      groupSet << groupTwo
      f.print "groupSet.size:",groupSet.size
      f.puts
      count = 0
      articleNumberSet = []
      f.print "cut: ",m_cut
      f.puts
      groupSet.each do |g|
        f.print "("
        f.print g.articles.size
        count += g.articles.size
        articleNumberSet << g.articles.size
        f.print ") - ["
        g.articles.each do |a|
          f.print a.id,", "
        end
        f.print "]\n"
      end
      f.printf "count: %d\n",count
      f.print "\n",m_cut*articleNumberSet.size
      f.close
    end
  when "Karger"    
    # if $DEBUG then $-f.puts "\nKarger's algorithm:" end
    puts "\nKarger's algorithm:"
    # min_cut
    m_cut, groupOne, groupTwo = Karger articles,concept,"max"

    
  end
  puts "\nfinish!"
  # if $DEBUG then $-f.puts "finish!" end

  # if min-cut less then throushould, concept should be split
  # min_cut = 0
  if m_cut > $SPLIT_THRES
    return true, m_cut, groupOne, groupTwo
  end
  print "m_cut: ",m_cut
  print "\n return false"
  false
end

def article_weight(concept)
  # if $DEBUG then $-f.write "\n" 'article_weight' end
  total_user = [];
  concept.articles.each do |a|
    total_user = total_user | a.users
  end
  sum_article_weight = 0.0
  concept.articles.each do |a|
    a.weight[concept.id] = a.users.length / total_user.length.to_f
    sum_article_weight += a.weight[concept.id]
  end

  user_concept_weight(concept, total_user, sum_article_weight)
end

def user_concept_weight concept, total_user, sum_article_weight
  # $-f.write "\n" 'user_concept_weight'
  cId = concept.id
  total_user.each do |u|
    # if $DEBUG_more
    #   $-f.write "\nu.id: "
    #   $-f.write u.id
    #   $-f.write "\nu.articleIDs: "
    #   $-f.write u.articleIDs
    # end
    sum_user_article_weight = 0.0
    concept.articles.each do |a|
      if u.articleIDs.include?(a.id)
        # to be determined
        sum_user_article_weight += a.weight[cId]
      end
    end
    u.conceptSet[concept.id] = sum_user_article_weight / sum_article_weight
  end
end

    ##########      #####      #####
   ############     #####      #####
  #####      ####   #####      #####
#####        #####  #####      #####
#####        ####   #####      #####
#####               #####      #####
#####               #####      #####
#####               #####      #####
#####       #####   #####      #####
#####       #####   #####      #####
#####       #####   #####      #####
#######   #######   #####      #####
 ##############      ################
     #######           ########  ######

def weight_common_user(concept, ai, aj)
  # sum the weight of user on this concept
  # the less ditance of this user, the more he influent
  sum_weight = 0.0;
  (ai.users & aj.users).each do |u|
    # if $COMMON_USER_SUM
      sum_weight += u.conceptSet[concept.id] # get concept weight for this user
    # else
    #   sum_weight += -1*u.conceptSet[concept.id] # the less seen article, the more important
    # end
  end
  if $DEBUG_more
    $f.write "\n"
    $f.write ai.id
    $f.write " -- "
    $f.write aj.id
    $f.write "\nsum_weight: "
    $f.write sum_weight
  end
  if sum_weight < 0
    print "\nsum_weight < 0!!\n"
  end
  # if not $COMMON_USER_SUM
  #   return sum_weight + $WEIGHTLEVLE 
  # else $COMMON_USER_SUM
    return sum_weight
  # end
end

def link_article(w,ai,aj)
  if $DEBUG_more
    $f.write "\nlink_article("
    $f.write w
    $f.write ", "
    $f.write ai.id
    $f.write ", "
    $f.write aj.id
    $f.write ")\n"
  end
  ai.add_link_article(w,aj)
  aj.add_link_article(w,ai)
  return ai, aj
end

# method for min-cut


# Edmonds_Karp
def Edmonds_Karp(a_source, a_tink, articles)
  $f.write "\nEdmonds_Karp function"
  flow = Hash.new(0)
  capacity = Hash.new(0)
  remain = Hash.new(0)
  $f.write "\n" 'Edmonds_Karp setting start:'
  articles.each do |ai|
    aa = ai.link_articles
    aa.keys.each_with_index do |linked_a, index|
      $f.write '.'
      if index+1 % 50 == 0
        $f.write "\n"
      end
      aj_id =  linked_a.id
      capacity[[ai.id,aj_id]] = aa[linked_a]
      capacity[[aj_id,ai.id]] = aa[linked_a]
      remain[[ai.id,aj_id]] = aa[linked_a]
      remain[[aj_id,ai.id]] = aa[linked_a]
    end
    # $f.write "\n"
  end
  $f.write "\n" 'Edmonds_Karp setting finish.'
  max_flow = 0
  
  s = a_source
  t = a_tink
  $f.write "\n" '=== find max-flow start: ==='
  printf "\n" '=== find max-flow start: ==='
  while 1 do
    $f.write "\n\nBFS#"
    path, df = BFS(s, t, remain)
    $f.printf "\nget df = %f\n",df
    if df == 0 then break end

    i = path[t.id]
    j = t.id
    
    while i != j do
      $f.write '!'
      flow[[i,j]] += df
      flow[[j,i]] += - flow[[i,j]]
      remain[[i,j]] = capacity[[i,j]] - flow[[i,j]]
      remain[[j,i]] = capacity[[j,i]] - flow[[j,i]]
      j = i
      i = path[j]
    end

    max_flow += df
  end
  $f.write "\n"
  $f.write "\n=== find max-flow end ==="
  printf "\n=== find max-flow end ==="

  return max_flow, flow, capacity, remain, path
end

def BFS(a_source, a_tink, remain)
  path = {}
  bfs_flow = {}
  visit = []
  queue = []

  s = a_source.id
  t = a_tink.id
  path[s] = s
  bfs_flow[s] = 1e9
  visit << s
  queue << a_source

  while not queue.empty?
    article = queue.shift
    $f.write "\n"
    $f.write article.id
    $f.write " *-* \n"
    index = 0
    article.link_articles.keys.each do |a|
      $f.write '^'
      id = article.id
      if not visit.include?(a.id) and remain[[id,a.id]] > 0
        visit << a.id
        path[a.id] = id
        bfs_flow[a.id] = [bfs_flow[id], remain[[id,a.id]]].min
        queue << a
        if a.id == a_tink.id
          return path, bfs_flow[a.id]
        end
      end
      index += 1
    end
  end
  return path, 0
end

