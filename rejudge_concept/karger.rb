def Karger articles,concept,flag
  if articles.size==1
   return -Float::INFINITY,nil,nil
 end
  if flag.nil?
    print "\nflag.nil!!"
    return
  end
  if flag == 'min'
    print "\n#choose the minimum weight"
  end
  groupOne = nil
  groupTwo = nil
  index = 0
  print "articles.length: ",articles.length ,"\n"
  min_cut = Float::INFINITY
  max_cut = -1*Float::INFINITY
  for i in 0...$REPEAT do
    groupSet = []
    groupSet_tmp = []
          
    articles.each_with_index do |a, i|
      groupSet_tmp << Group.new(a)
    end
    
    # if $DEBUG then $-f.puts "\nlinked group..." end
    # puts "\nlinked group..."
    # linked group is the same to linked article
    add_weight = 0.0
    groupSet_tmp.each do |g|
      g.articles.first.link_articles.each do |hash|
        a = hash[0]
        g_linked = groupSet_tmp.find {|x| x.articles.first == a}
        if not g_linked.nil?
          if $POSTAVERAGE or $COMMONAVERAGE
            g.link_groups[g_linked] = hash[1]
          elsif $PREAVERAGE # prev abstruct
            add_weight += hash[1]
          end
        end
      end
      # $f.print "-> ",g.link_groups.size
      if $POSTAVERAGE or $COMMONAVERAGE then groupSet << g end
    end
    
    if $PREAVERAGE # prev abstruct
      average = add_weight/2.0/articles.size
      groupSet_tmp.each do |g|
        g.articles.first.link_articles.each do |hash|
          a = hash[0]
          g_linked = groupSet_tmp.find {|x| x.articles.first == a}
          if not g_linked.nil?
            g.link_groups[g_linked] = hash[1] - average              
          end
        end
        groupSet << g
      end
    end 
    # printOut(groupSet,"after linked")
    # if $DEBUG then $-f.puts "linked groups end" end

    # random choose article and merge the heightest weight
    # neighbor
    # groupSet_initial = groupSet
    printf "*"
    # if $DEBUG
    #   printOutGroup groupSet,"before REPEAT,groupSet:"
    #   printOutGroup groupSet,"#{i} groupSet"
    #   $-f.puts
    # end
    linked_group = groupSet.clone

    #############          #######
    #############          #######
    #####             ###################
    #####             ###################
    ###############        #######
    ###############        #######
              #####        #######
              #####        #######
    ###############        ##############
    ###############        ##############

    # if $DEBUG then $-f.printf "\n===== start merge...======\n" end
    # printf "\n===== start merge...======\n"
    index += 1
    # random select a super node until number of group 
    # reduces to two
    # or until there's no link between two groups 
    while groupSet.size > 2
      printf ">"
      # if $DEBUG 
      #   $-f.printf "\n\n===== new iteration ===== \n"
      #   $-f.printf "index- %d merges:\n",index
      #   # printOut(groupSet,"while:")
      # end

      if linked_group.empty?
        puts "break! linked_group.empty"
      #   $-f.puts "break!"
      end
      break if linked_group.empty?
        
      

      # random select a group
      g_sel = linked_group.sample
      
      # ====debug====
      # $-f.print "\ng_ind:",g_ind
      # if $DEBUG 
      #   $-f.print "\ngroupSet.size:",groupSet.size
      #   $-f.print "\nlinked_group.size:",linked_group.size

      #   $-f.print "\nrandom select group: ["
      #   g_sel.articles.each do |a|
      #     $-f.print " ",a.id
      #   end
      #   $-f.print " ]"
      #   printOutGroup groupSet
      # end
      # if no linked choose another one
      if not g_sel.link_groups.size > 0
        # if $DEBUG 
        #   $-f.print "\nno linked group!\n"
        #   $-f.print "x["
        # end
        print "x["
        g_sel.articles.each do |a|
          print a.id," "
          # if $DEBUG then $-f.print a.id," " end
        end
        # if $DEBUG then $-f.print "];" end
        linked_group.delete(g_sel)
      end
      if not g_sel.link_groups.size > 0
        print "\ng_sel.link_groups.size == 0"
        print "\nlinked_group.size: ",linked_group.size
        print "\ngroupSet.size: ",groupSet.size,"\n"
      end
      next if not g_sel.link_groups.size > 0

      groupSet.delete(g_sel)
      linked_group.delete(g_sel)


      #######################################
      # write out message
      # if $DEBUG 
      #   $-f.print "sorted linked group ...\n the linkedgroup:\n"
      #   g_sel.link_groups.each do |g|          
      #     $-f.print "[ "
      #     g[0].articles.each do |a|
      #       $-f.print a.id,","
      #     end
      #     $-f.print " ]- "
      #     $-f.print g[1]
      #     $-f.print "; "
      #   end
      # end
      #######################################
      
      # select the most weighted linked group
      linked_g_sorted = (g_sel.link_groups).sort_by {|k,v| -1*v}
      #######################################
      # write out message
      # if $DEBUG 
      #   $-f.print "sorted end"
      #   $-f.print "\nlinked_g_sorted: [ "
      #   linked_g_sorted.each do |g|
      #     $-f.print "[ "
      #     g[0].articles.each do |a|
      #       $-f.print a.id,","
      #     end
      #     $-f.print " ], "
      #   end
      #   $-f.print "]"
      #   $-f.print "\nbe linked: ["
      #   (linked_g_sorted.first)[0].articles.each do |a|
      #     $-f.print a.id,","
      #   end
      #   $-f.print " ]\n"
      #   # printOut(groupSet,string="before delete")
      # end
      ####################################### 
      if flag == 'max' #choose the maxmimum weight
        gg = (linked_g_sorted.first)[0]
      elsif flag == 'min' #choose the minimum weight
        gg = (linked_g_sorted.last)[0]
      end

      g_chosen = groupSet.find {|x| x.articles == gg.articles}
      
      # if $DEBUG 
      #   if g_chosen.nil?
      #     $-f.puts "!!!"
      #   end
      #   $-f.puts  "g_chosen.articles.size:"
      #   $-f.puts g_chosen.articles.size
      # end
      
      groupSet.delete(g_chosen)
      linked_group.delete(g_chosen)
      
      # if $DEBUG 
      #   # printf "\nlinked_g_sorted ::after : \n"
      #   # print linked_g_sorted
      #   $-f.printf "\ngroupSet.size 2: %d\n", groupSet.size
      #   $-f.print "mergeTwoGroups start... " 
      # end
      
      gmerged = mergeTwoGroups g_sel, g_chosen
      
      # if $DEBUG 
      #   $-f.print "mergeTwoGroups end \n"
      #   $-f.print Time.now
        
      #   $-f.print "\nrenewGroups start "
      # end
      groupSet = renewGroups groupSet,g_chosen,g_sel,gmerged
      
      # if $DEBUG then $-f.print "\nrenewGroups end " end

      groupSet << gmerged
      linked_group << gmerged
      
      if $DEBUG
        printOutGroup groupSet
        printOut groupSet
        # $-f.puts "\n ===iteration ends ===\n" 
      end
    end      
    # recored the current groups before become two
    cut_num = 0
    if groupSet.size == 2
      cut_num = groupSet[0].link_groups[groupSet[1]]
    end
    

    print "\ngroupSet.size: ",groupSet.size
    cut,groupSet = cut_of_groups groupSet
    print "\ncut: ",cut

    if cut.nil? then cut = 0 end
    # f.print "\n after merge:\n"
    # groupSet.each do |g|
    #   f.print "("
    #   f.print g.articles.size
    #   count += g.articles.size
    #   articleNumberSet << g.articles.size
    #   f.print ") - ["
    #   g.articles.each do |a|
    #     f.print a.id,", "
    #   end
    #   f.print "]\n"
    # end
    

    # f.puts "cut:",cut
    # f.close
    # if $DEBUG 
    #   $-f.puts "cut:",cut
    #   $-f.puts "min-cut:",min_cut
    # end
    if flag == 'max' #choose the maxmimum weight        
      if cut < min_cut
        min_cut = cut
        
        # if $DEBUG
        #   puts "min_cut:",min_cut
        #   $-f.puts "cut < min_cut!"
        #   if not groupSet[0].nil?
        #     $-f.write "\ngroupOne: \n"
        #     groupSet[0].articles.each do |a|
        #       $-f.print a.id," "
        #     end
        #   end
        #   if not groupSet[1].nil?
        #     $-f.write "\ngroupTwo: \n"
        #     groupSet[1].articles.each do |a|
        #       $-f.print a.id," "
        #     end
        #   end
        # end

        groupOne = groupSet[0]
        groupTwo = groupSet[1]
      end
    elsif flag == 'min' #choose the minimum weight
      if cut > max_cut
        print "\ncut > max_cut!\n"
        max_cut = cut
        groupOne = groupSet[0]
        groupTwo = groupSet[1]
      end
    end
  end
  if flag == 'max'
    return min_cut, groupOne, groupTwo
  elsif flag == 'min'
    print "return max_cut: ",max_cut," ",groupOne.articles.size," ",groupTwo.articles.size
    return max_cut, groupOne, groupTwo
  end
end

#############     ##############
#############     ##############
#############     ##############
#######  ######  ######  #######
#######  ######  ######  #######  
#######  ######  ######  #######
#######  ######  ######  #######
#######  ######  ######  #######
#######  ##############  #######
#######   ############   #######
#######                  #######
#######                  #######
#######                  #######
def mergeTwoGroups g, g_chosen
  link_groups1 = g.link_groups
  link_groups2 = g_chosen.link_groups
  
  # the name in both link_groups should be deleted
  link_groups1.delete(g_chosen)
  link_groups2.delete(g)
  # same groups linked to these two groups 
  # the weight should be averaged;
  # the result is recorded in linke_groups1 
  link_groups1.keys.each do |k|
    if link_groups2.has_key? k
      if $POSTAVERAGE
        a = (link_groups1[k] + link_groups2[k])/2.0
        link_groups1[k] = a
      elsif $COMMONAVERAGE or $PREAVERAGE
        link_groups1[k] += link_groups2[k]
      end
    end
  end
  # the articels in both groups should be added
  # the result is recorded in  g_chosen
  a = g_chosen.articles + g.articles
  g_chosen.articles = a
  g_chosen.articles.uniq # remove the same article
  # merge the link_groups
  g_chosen.link_groups = link_groups2.merge(link_groups1)
  return g_chosen 
end


################      ##########     ###
#################     ##########     ###
######     ######     ##########     ###
######     ######     ##########     ###
######     ######     ############   ###
#################     ######   ##### ###
#################     ######   #########
######       ######   ######    ########
######       ######   ######    ########
######       ######   ######     #######
######       ######   ######      ######
######       ######   ######      ######
######       ######   ######      ######

def renewGroups groupSet,g_chosen,g_sel,gmerged
  # renow other group linked to this two groups
  groupSet_tmp = groupSet.clone
  groupSet = []
  # if $DEBUG
  #   printOutGroup groupSet_tmp,"renew,group_tmp"
  #   $-f.print "\ndelete the linked group in other groups\n"
  # end
  # g_chosen.link_groups
  groupSet_tmp.each do |g|
    # if g_link_groups.class == Hash
    #   puts "g_link_groups.is_a a H!!"
    # end
    if g.link_groups.has_key?(g_chosen) or g.link_groups.has_key?g_sel
      # if $DEBUG
      #   $-f.print "\n["
      #   g.articles.each do |a|
      #     $-f.print a.id," "
      #   end
      #   $-f.print "]"
      #   $-f.print "\ndelete!!;"
      #   $-f.print "\nbefore del:"
      #   g.link_groups.each do |g|
      #     $-f.print "["
      #     g[0].articles.each do |a|
      #       $-f.print a.id," " 
      #     end
      #     $-f.print "]"
      #   end

      #   $-f.print "\ngmerged: ["
      #   gmerged.articles.each do |a|
      #     $-f.print a.id," "
      #   end
      #   $-f.print "]"
      # end
      
      (g.link_groups).delete(g_chosen) 
      (g.link_groups).delete(g_sel)

      # if $DEBUG then $-f.print "\nafter del: g.link_groups.size: ",g.link_groups.size end
      if not gmerged.link_groups[g].nil?
        # $f.print "\nnot gmerged.link_groups[g].nil?\n"
        g.link_groups[gmerged] = gmerged.link_groups[g]
      elsif g.link_groups.has_key?(g_chosen)
        # $-f.print "\ng.link_groups.has_key?(g_chosen)\n"
        g.link_groups[gmerged] = g.link_groups[g_chosen]
      elsif g.link_groups.has_key?(g_sel)
        # $-f.print "\ng.link_groups.has_key?(g_sel)\n"
        g.link_groups[gmerged] = g.link_groups[g_sel]
      end

      # if $DEBUG
      #   g.link_groups.each do |g|
      #     $-f.print "["
      #     g[0].articles.each do |a|
      #       $-f.print a.id," " 
      #     end
      #     $-f.print "] "
      #   end
      #   $-f.print "\ngmerged.link_groups.size: ",gmerged.link_groups.size
      #   $-f.print "\ngmerged.link_groups[g]: ",gmerged.link_groups[g]
      # end
    end
    groupSet << g
  end
  groupSet
end