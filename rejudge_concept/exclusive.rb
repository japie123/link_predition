print "r_c/exclusive\n"
def exclusive concept,articles 
  # initial
  groupOne = nil
  groupTwo = nil
  groupSet_tmp = []
  groupOne = Group.new(articles.first)
  articles.each_with_index do |a, i|
    next if i == 0
    groupOne.articles << a
  end
  g1_as = groupOne.articles
  g1_as.each do |article|
    article.link_articles.each do |hash|
      a = hash[0]
      if g1_as.include? a then article.self_link += hash[1] end
    end
  end

  # check if there's any link in graph
  # otherwise merge using VSM
  sum_self = 0.0
  articles.each do |a| 
    sum_self += a.self_link
  end
  if sum_self == 0.0
    print "\nsum_self == 0 "
    groupSet = []
    max_cut = 0
    articles.each do |a|
      groupSet << Group.new(a)
    end
    if not $B4MERGE
      if groupSet.size > 2
        max_cut,groupSet = cut_of_groups groupSet
      else
        groupOne = groupSet[0]
        if groupSet.size == 2
          groupTwo = groupSet[1]
        else
          max_cut = -1
        end
      end
    end
  else
    print "\ngreedy seach max cut: "
    # if there's link, greedy find max cut
    articles = groupOne.articles.clone
    a_sort = articles.sort_by {|x| -1*x.self_link}
    first_a = a_sort.first
    first_a.self_link,first_a.other_link = first_a.other_link,first_a.self_link
    first_a.side = 1
    groupTwo = Group.new(first_a)
    groupOne.articles.delete(first_a)
    
    # if concept.id == 40 or concept.id == 18
    #   binding.pry
    # end
    max_cut = 0
    articles.each do |a|
      max_cut += a.other_link
    end
    # max_cut is sum of other_linke divided by 2
    max_cut /= 2
    resever = groupOne.articles.clone
    while 1
      print "1,"      
      a_sort = resever.sort_by {|x| -1*x.self_link}
      aind = a_sort.first
      # break if index == articles.size
    
      # if concept.id == 40
      #   print "articles.size: ",articles.size
      #   print "\naind: ",aind
      #   binding.pry
      # end
      # for self, swap variables values
            
      aind.link_articles.each do |hash|
        lined_a = hash[0]
        if lined_a.side == 0
          lined_a.self_link -= lined_a.link_articles[aind]
          lined_a.other_link += lined_a.link_articles[aind]
        else
          lined_a.self_link += lined_a.link_articles[aind]
          lined_a.other_link -= lined_a.link_articles[aind]
        end        
      end
      cut = 0
      articles.each do |a|
        cut += a.other_link
      end
      # cut is sum of other_linke divided by 2
      cut /= 2
      
      if cut > max_cut
        max_cut = cut
        aind.self_link,aind.other_link = aind.other_link,aind.self_link
        aind.side = 1
        groupOne.articles.delete(aind)
        groupTwo.articles << aind
      end
      resever.delete(aind)
      break if resever.empty?
    end
    print "max_cut: ",max_cut
    groupSet = []
    groupSet << groupOne
    groupSet << groupTwo
  end
  # Dir.mkdir("#{$PRE}split/groupb4Merge_#{ARGV[0]}") unless File.exist?"#{$PRE}split/groupb4Merge_#{ARGV[0]}"
  # f = File.new("#{$PRE}split/groupb4Merge_#{ARGV[0]}/groupb4Merge_#{concept.id}_t0","w")
  # f.print "groupSet.size:",groupSet.size
  # f.puts
  # count = 0
  # f.print "max_cut: ",max_cut
  # print "max_cut: ",max_cut
  # f.puts
  # groupSet.each do |g|
  #   count += g.articles.size
  #   f.print "(",g.articles.size,") - ["
  #   g.articles.each do |a|
  #     f.print a.id,", "
  #   end
  #   f.print "]\n"
  # end
  # f.printf "count: %d\n",count
  # f.close
  return max_cut, groupOne, groupTwo
end