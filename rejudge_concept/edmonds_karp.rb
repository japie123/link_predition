puts "edmonds_karp.rb"
def Edmonds_Karp articles
  # calculate min-cut
  # s = concept.articles.sample
  # begin t = concept.articles.sample end while s.id == t.id
  groupOne = nil
  groupTwo = nil
  i = rand(articles.length)
  flag = false
  for j in 0..articles.length-1
    $f.write 'm'
    next if j == i
    cut, gOne, gTwo = minimum_s_t_cut(articles[i], articles[j], articles)
    if $DEBUG
      $f.write "\ncut:"
      $f.write cut
      $f.puts
    end
    if cut < min_cut
      min_cut = cut
      groupOne = gOne
      groupTwo = gTwo
      $f.puts "oyoooooo"
      $f.print groupOne
      $f.printf "\n"
      $f.print groupTwo
    end
    # if cut == 0 then flag = true end
    # if flag then break end
    # if DEBUG
    #   break
    # end
  end
  return min_cut, groupOne, groupTwo
end

def minimum_s_t_cut(a_source, a_tink, articles)
  $f.write "\n\n =*==*==*==*==*= "
  $f.write "\nminimum_s_t_cut:"
  $f.write "\na_source.id: "
  $f.write a_source.id
  $f.write "\na_tink.id: "
  $f.write a_tink.id
  $f.write "\n"
  puts Time.now
  printf "\n === Edmonds_Karp start ===\n"
  $f.write "\n === Edmonds_Karp start ==="
  max_flow, flow, capacity, remain = Edmonds_Karp(a_source, a_tink, articles)
  printf "\n === Edmonds_Karp end ===\n"
  $f.write "\n === Edmonds_Karp end ===\n"
  visit = []
  a_len = articles.length
  $f.write "\n -- DFS start --\n"
  visit, path = DFS a_source, visit, remain, articles, []
  $f.write "\n -- DFS end --"
  if $DEBUG
    $f.write "\n"
    $f.write "articles.length: "
    $f.write articles.length
    $f.write "\n\nflow:"
    $f.write flow
    $f.write "\nremain:\n"
    $f.write remain
  end

  cut_edge = []
  groupOne = []
  groupTwo = []
  articles.each do |a|
    if not visit.include? a.id
      groupOne << a.id
    else
      groupTwo << a.id
    end
  end
  if $DEBUG
    $f.write "\ngroupOne: "
    $f.write groupOne
    $f.write "\ngroupTwo: "
    $f.write groupTwo
    $f.puts "\n =*==*==*==*==*==*= \n"
  end
  return max_flow, groupOne, groupTwo
end

def DFS article, visit, remain, articles, path
  visit << article.id
  if $DEBUG
    $f.write "\narticle.id:"
    $f.write article.id
    $f.write "\nvisit: "
    $f.write visit
    if $DEBUG_more
      article.link_articles.keys.each do |j|
        $f.write "\n"
        $f.write j.id
      end
    end
  end
  article.link_articles.keys.each do |j|
    j_id = j.id
    if not visit.include?(j_id) and remain[[article.id,j_id]] > 0
      if $DEBUG then $f.printf "\n from: %d\n",article.id end
      visit, path = DFS j, visit, remain, articles, path
    else
      path << {j_id => article.id}
    end
  end
  return visit, path
end