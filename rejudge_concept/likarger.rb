# load 'node.rb'
# require 'yaml'
# require 'pry'

# articleSet = YAML.load_file("cache/articleSet_interest_rand100")

# max_cut, groupOne, groupTwo = liKager articleSet
print "\nliKarger!"


def liKarger articles
  if articles.size==1 then return -Float::INFINITY,nil,nil end
  max_cut = 0.0
  g1 = nil
  g2 = nil
  i = 0
  10.times do
    print i,": " 
    # random select half
    groupOne = Group.new()
    groupTwo = Group.new()
    articles.each do |a|
      if rand(2) > 0
        a.side = 0
        groupOne.articles << a
      else
        a.side = 1
        groupTwo.articles << a
      end
    end

    # sum the link
    articles.each do |article|
      ss = article.side
      article.self_link = 0
      article.other_link = 0
      article.link_articles.each do |hash|
        a = hash[0]
        if a.side == ss
          article.self_link += hash[1] 
        else 
          article.other_link += hash[1] 
        end 
      end
    end

    # initial cut
    this_cut = 0
    articles.each do |a|
      this_cut += a.other_link
    end
      # cut is sum of other_linke divided by 2
    this_cut /= 2 
    print "\nthis_cut: ",this_cut


    while 1 
      print "^"      
      # chose the node make the maximum difference
      chosen_a = nil
      side = nil
      max_diff = 0 
      articles.each do |a|
        _cut = a.self_link - a.other_link
        if _cut > max_diff
          max_diff = _cut
          chosen_a = a
          side = a.side
        end
      end
      print "\ndiff: ",max_diff

      # This means no node change improves the cut
      if chosen_a.nil?
        # Then should examine if this cut exceeds the overall maximum in 10 times
        print "\nmax_cut: ",max_cut
        if this_cut > max_cut
          max_cut = this_cut
          g1 = groupOne
          g2 = groupTwo
        end
        print "\n"
        i += 1
        break
      end


      # Find the change node, chosen_a, exchange node and renew the cut 
      this_cut += max_diff
      chosen_a.self_link,chosen_a.other_link = chosen_a.other_link,chosen_a.self_link
      chosen_a.link_articles.each do |hash|
        lined_a = articles.find{|x| x == hash[0]}
        val = lined_a.link_articles[chosen_a]
        if lined_a.side == side # node in the same side
          lined_a.self_link -= val
          lined_a.other_link += val
        else
          lined_a.self_link += val
          lined_a.other_link -= val
        end        
      end
      if side == 0
        groupOne.articles.delete(chosen_a)
      else
        groupTwo.articles.delete(chosen_a)
      end
      chosen_a.side = (chosen_a.side+1) % 2
      if chosen_a.side == 0
        groupOne.articles << chosen_a
      else
        groupTwo.articles << chosen_a
      end
      
    end
  end
  print "\nreturn max_cut: ", max_cut
  return max_cut, g1, g2
end