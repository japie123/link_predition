def cut_of_groups groupSet
  # printf "\ncut_of_groups: groupSet.size: %d\n",groupSet.size
  # if $DEBUG then $-f.printf "\ncut_of_groups: groupSet.size: %d\n",groupSet.size end
  index = 0

  # merge those group which are nearest
  if groupSet.size > 2
    # $-f.print "\ngroupSet.size>2: ",groupSet.size
    # turn each group into a vector 
    group_point_map = {}
    group_point_set = []
    groupSet.each_with_index do |g,ind|
      print "\n",ind,"\n"
      # $-f.print ind," "
      point = {}
      g.articles.each do |a|
        vector = YAML::load_file("#{$PRE}articles_vector/"+a.id.to_s)
        point.merge!(vector) { |k,v1,v2| v1.to_f+v2.to_f }
      end
      # $-f.print "\npoint: \n",point
      article_num = g.articles.length
      point.each {|k,v| point[k] = v/article_num }
      # $-f.print "\naverage point: \n",point
      # print "\npoint: \n",point
      group_point_set << Article.new(ind,point)
      group_point_map[ind] = g
    end
    # print "\ngroup_point_map: ",group_point_map
    # $f.print "\ngroup_point_map: ",group_point_map

    # k-means to combine groups
    groupSet = []
    clusters = k_means group_point_set, 2, $DELTA
    clusters.each do |a|
      g = group_point_map[a.id]
      groupSet << g
    end
    # printOutGroup groupSet,"after clusters"
    return 0, groupSet
  end
  print "\ncut_of_groups merge end\n"
  # polling each article in one group
  # if article belonged to one group has neighbor in another,
  # add the weight
  # groupOneID = group2articles.first[0]
  # articles = group2articles.first[1]
  # cut = 0.0
  # articles.each do |a|
  #   a.link_articles.each do |linked_a|
  #     if article2group[linked_a.key.id] != groupOneID
  #       cut += linked_a.weight
  #     end
  #   end
  # end
  # if $DEBUG then $-f.puts "\nreturn cut_of_groups: ",groupSet[0].link_groups[groupSet[1]] end
  return groupSet[0].link_groups[groupSet[1]], groupSet
end


def printOut(groupSet,string="")
  # debug: link group
  # $-f.puts string
  # $-f.puts "groupSet-all:"
  groupSet.each do |g|
    # $-f.print "{<"
    g.articles.each do |a|
      # $-f.print a.id," * "
    end
    # $-f.print ">-["
    g.link_groups.each do |gg|
      # $-f.print "("
      gg[0].articles.each do |a|
        # $-f.print a.id,","
      end
      # $-f.print ")-",gg[1]," | "
    end
    # $-f.print "];"
    # $-f.print "};\n"
  end
end

def printOutGroup groupSet,str=""
  # $-f.print "\n#{str}: [ "
  groupSet.each do |g|          
    # $-f.print "[ "
    g.articles.each do |a|
      # $-f.print " ",a.id,","
    end
    # $-f.print " ], "
  end
  # $-f.print "]"
end
