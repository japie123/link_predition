
print "ruby pd_eq7.rb v# cache/conceptSet cache/userSet"

def predict conceptSet, userSet, prob, new_aids
print "\nconceptSet.size: ",conceptSet.size
print "\nuserSet.size: ",userSet.size
print "\ndecide_user_concept..."
userSet2 = decide_user_concept(conceptSet, userSet,"")
userSet_predict = []
totally_count = []
countnil=0
print "\nuserSet2.size: ",userSet2.size
userSet2.each_with_index do |u,i|
  count = 0
  if i%1000 == 0
    print "\n",Time.now 
    print "\n",i," " 
  end
  if i%1 == 0 then print "." end
  article_score = {}
  numerator = {}
  denominator = {}
  denominator_2 = {}
  u.conceptSet.each do |c|
    c_id = c[0]
    uc_degree = c[1]
    if uc_degree.nan?
      binding.pry
    end
    # uc_degree = u.conceptSet[c]
    concept = conceptSet.find {|x| x.id == c_id}
    next if c.nil?
    if concept.nil?
      countnil+=1
      next
    end
    avgW = 0.0
    countW = 0
    avgP = 0.0
    countP = 0
    concept.articles.each do |a|
      if not a.weight[c_id].nil?
        avgW += a.weight[c_id]
        countW +=1
      end
      if not prob[a.id].nil?
        avgP += prob[a.id]
        countP +=1
      end
    end
    countW /= avgW
    countP /= avgP
    concept.articles.each do |a|
      if new_aids.include?a.id
        # print "!"
        count+=1
      end
      # if prob[a.id].nil? then prob[a.id]=0.01 end
      # if prob[a.id].nil?
      #   print "! "
      # end
      if new_aids.include?a.id and a.concepts[c_id].nil?
        binding.pry
      end
      if a.concepts[c_id].nil?
        print "x"
        next
      end
      if prob[a.id].nil?
        prob[a.id] = avgP
      end
      w = 0
      if a.weight[c_id].nil?
        w = avgW
      else
        w = a.weight[c_id]
      end

      numerator[a.id] ||= 0
      numerator[a.id] += uc_degree*w*prob[a.id]
      denominator[a.id] ||= 0
      denominator[a.id] += (a.concepts[c_id]*prob[a.id])**2
      denominator_2[a.id] ||= 0
      denominator_2[a.id] += uc_degree**2
    end
    # binding.pry
  end
  numerator.each do |a_h|
    a_id = a_h[0]
    numer = a_h[1]
    sc = numer.to_f/denominator[a_id]/denominator_2[a_id]
    if sc.nan?
      next
    end
    article_score[a_id] = sc
  end
  next if article_score.size ==0
  u_new = User.new(u.id)
  # print article_score.size
  # sort = article_score.sort_by {|k,v| -1*v}
  sort = article_score.sort_by {|k,v| -1*v}
  sort.each do |s|
    next if u.articleIDs.include?s[0]
    u_new.articleIDs << s[0]
  end
  # print "%",u_new.articleIDs.size," "
  userSet_predict << u_new
  totally_count << count
end
userSet_predict
end

# f= File.new("cache/userSet_predict_eq7_#{ARGV[0]}","w")
# f.puts userSet_predict.to_yaml
# f.close
# binding.pry