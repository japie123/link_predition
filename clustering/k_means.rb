def k_means  (articleSet, k = 5,  delta = 0.01)
  if articleSet.empty?
    puts '[Error!] articleSet is empty!'
  end
  k = articleSet.length if articleSet.length < k
  clusters = []
  count = Set.new
  while count.size < k
    print "."
    count << rand(articleSet.size)
  end
  i = 1
  count.each do |c|
    clusters << Cluster.new(i,articleSet[c].tags)
    i += 1
  end
  # Test
    # a = Article.new()
    # b = Article.new()
    # for j in 0..10
    #   a.tags[j] = 1
    #   b.tags[j] = 0
    # end
    # clusters << Cluster.new(a)
    # clusters << Cluster.new(b)

  # puts 'clusters, center , tags'
  # puts clusters
  # clusters.each do |cluster|
  #   puts cluster.id
  #   puts cluster.center.tags
  # end
  # puts 'end2'
  # puts
  # print clusters
  iteration = 0 # only for record
  # puts 'k_means: clusers[0].id'
  # puts clusters[0].id
  # puts clusters[0].number_of_users
  while clusters.any?(&:moved)
    printf "\n\nnew iteration:"
    clusters.each(&:reset)
    # puts '=== reset ==='  
    articleSet.each_with_index do |a, index|
      if index%1000==0 then printf "\n%d ",index end
      if index%10==0 then printf "." end
      shortest_dist = Float::INFINITY 
      cluster_belonged = nil
      clusters.each do |c|
        # puts c.center.tags
        distance = a.distance_to(c.center)
        if distance < shortest_dist
          shortest_dist = distance
          cluster_belonged = c
          # puts c.number_of_users
          # print 'cluster_belonged.id: ',cluster_belonged.id
          # puts
        end
      end
      cluster_belonged.addArticle(a) unless cluster_belonged.nil?
    end
    # puts
    # puts '=== 1 ==='

    # if there is empty cluster
    clusters.delete_if{|cluster| cluster.number_of_article == 0 }
    # delete = 0
    # clusters.each do |cluster|
    #   if cluster.number_of_article == 0 
    #     delete =  delete + 1
    #   end
    # end
    # puts 'delete', delete
    # reassign center
    clusters.each { |cluster| cluster.update_center delta}
    # clusters.each do |cluster|
    #   print 'cluster.center.tags: ',cluster.center.tags
    #   puts
    # end
    
    # print iteration
    # print ' - clusters: '
    # print clusters.length
    # puts
    # puts '=== iteration ends ==='
    # puts
    # puts
    # record iteration
    iteration += 1
  end
  # puts '========'
  puts "\niteration:",iteration
  puts "\nclusters.size: ",clusters.size
  # puts 'cluster'
  # puts clusters.length
  # clusters.each do |cluster|
  #   puts cluster.id
  #   puts cluster.center.tags
  # end
  # puts 'clusters',clusters
  return clusters
end