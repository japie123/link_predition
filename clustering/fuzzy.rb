puts "\nfuzzy.rb"
$EPS = 0.3
def fuzzy articleSet, k=5, delta=0.01
  f= File.new("data","w")
  allk = []
  articleSet.each do |a|
    a.tags.each do |t|
      allk << t[0]
    end
  end
  allk.uniq!
  print "\nallk: ",allk
  articleSet.each do |a|
    allk.each do |k|
      if a.tags.has_key?k
        f.print a.tags[k]," "
      else
        f.print 0," "
      end
    end
    f.print "\n"
  end
  f.close
  articleSet.each {|a| a.concepts = {}}

  `./ma.sh fun #{k}`
  f=File.open("U")
  ind = 0
  while (line = f.gets)
    arr = line.split(' ')
    arr.each_with_index do |u,i|
      articleSet[i].concepts[ind] = arr[i].to_f
    end
    ind += 1
  end
  f.close
  f=File.open("center")
  conceptSet = []
  center = []
  ind = 0
  while (line = f.gets)
    center << line
    c = Concept.new(ind)
    c.articles.replace articleSet
    conceptSet << c
    ind += 1    
  end
  # binding.pry
  # merge
  i = 0
  # while(i<conceptSet.size)
  #   ci = conceptSet[i]
  #   conceptSet.delete_if do |c|
  #     # binding.pry
  #     next if ci.id == c.id
  #     if dis(ci.center,c.center) < 0.0001
  #       print "\ndis < 0.0001!! "
  #       print ci.id,"<-",c.id
  #       ci.articles.each do |a|
  #         if a.concepts[c.id].nil?
  #           # binding.pry
  #         end
  #         a.concepts[ci.id] += a.concepts[c.id]
  #         a.concepts.delete c.id
  #       end
  #       # conceptSet.delete(c)
  #       true
  #     end
  #   end
  #   i+=1
  # end
  # binding.pry
  return conceptSet

  m = 2 # 1<= m < infinity
  # random select center
  k = articleSet.length if articleSet.length < k
  conceptSet = []
  count = Set.new
  while count.size < k
    print "."
    count << rand(articleSet.size)
  end
  i = 1
  count.each do |c|
    conceptSet << Concept.new(i,articleSet[c].tags)
    i += 1
  end

  # initila Uij
  # uij = 1/ sigma_{k=1,C}{ (|xi-cj|/|xi-ck|)^(2/m-1) }
  uijSet = []
  articleSet.each do |a|
    sum_c = 0.0
    conceptSet.each do |cj|
      sum = 0.0
      dij = dis(a.tags,cj.center)
      conceptSet.each do |ck|
        dik = dis(a.tags,ck.center)
        if (dij/dik).nan?
          sum += 1.0
        else
          sum += (dij/dik )**(2/(m-1))
        end
      end
      if sum > 0.0
        a.concepts[cj.id] = 1/sum
        cj.articles << a        
      else
        a.concepts[cj.id] = 1.0
      end
      sum_c += a.concepts[cj.id]
    end
    a.concepts.each do |h|
      h[1] /= sum_c
      a.concepts[h[0]] = h[1]
      uijSet << h[1]
    end
  end
  # binding.pry
  diff_pre = Float::INFINITY

  print "\nstart while: "
  iter =1
  while 1
    sum_uij = 0.0
    uijSet.each {|uij| sum_uij+=(uij**m)}
    # k-step, c_j  
    # c_j = sigma_{1,N}{u_ij^m * x_j } 
    #       ----------------------------
    #         sigma_{i=1,N}{u_ij^m}
    # print "k"
    conceptSet.each do |c|
      # sigma_{i=1,N}{u_ij^m}      
      centerTags = {}
      articleSet.each do |a|
        a.tags.each do |t|
          val = a.concepts[c.id]*t[1]
          centerTags[t[0]] = centerTags.has_key?(t[0]) ? centerTags[t[0]] + val : val
        end
      end
      centerTags.each {|k,v| v /= articleSet.size  }
      centerTags.each {|k,v| v /= sum_uij }      
      c.center = centerTags
      # print "\nc-center: ",c.center
    end
    
    # merge concepts
    # conceptSet_merge = []
    # conceptSet = conceptSet.clone
    # binding.pry
    i = 0
    while(i<conceptSet.size)
      ci = conceptSet[i]
      conceptSet.delete_if do |c|
        # binding.pry
        next if ci.id == c.id
        if dis(ci.center,c.center) < 0.0001
          print "\ndis < 0.0001!! "
          print ci.id,"<-",c.id
          ci.articles.each do |a|
            if a.concepts[c.id].nil?
              # binding.pry
            end
            a.concepts[ci.id] += a.concepts[c.id]
            a.concepts.delete c.id
          end
          # conceptSet.delete(c)
          true
        end
      end
      i+=1
    end
    conceptSet.each do |c|
      # sigma_{i=1,N}{u_ij^m}      
      centerTags = {}
      articleSet.each do |a|
        a.tags.each do |t|
          val = a.concepts[c.id]*t[1]
          centerTags[t[0]] = centerTags.has_key?(t[0]) ? centerTags[t[0]] + val : val
        end
      end
      centerTags.each {|k,v| v /= articleSet.size  }
      centerTags.each {|k,v| v /= sum_uij }      
      c.center = centerTags
      # print "\nc-center: ",c.center
    end
    # binding.pry

    # print "m"
    # m-step, update u_ij
    uijSet_next = []
    dist = {}
    articleSet.each do |a|
      dist[a.id] = []
      sum_c = 0.0
      conceptSet.each do |cj|
        cj.articles.delete(a) 
        sum = 0.0
        dij = dis(a.tags,cj.center)
        conceptSet.each do |ck|
          dik = dis(a.tags,ck.center)
          dist[a.id] << dik
          if (dij/dik).nan?
            sum += 1.0
          else
            sum += (dij/dik )**(2/(m-1))
          end
        end
        if sum > 0.0 
          # print "!" 
          a.concepts[cj.id] = 1/sum          
        else
          a.concepts[cj.id] = 1.0
        end
        sum_c += a.concepts[cj.id]
        cj.articles << a
      end
      a.concepts.each do |h|
        h[1] /= sum_c
        uijSet_next << h[1]
      end
    end

    # binding.pry
    # if Uij^(k+1) - Uij^k < eps
    diff = 0.0
    (0...uijSet_next.size).each do |i|
      diff += Math.sqrt((uijSet_next[i] - uijSet[i])**2)
    end
    # binding.pry

    # sum_{i=1,c}sum_{j=1,n}{uij^m * dist(ci,xj)^2}
    sum_j = 0.0
    articleSet.each do |a|
      conceptSet.each_with_index do |c,i|
        sum_j += a.concepts[c.id]*dist[a.id][i]
      end
    end

    # print diff,", sumj: "
    # print sum_j,"\n"
    # if diff < diff_pre
    #   print "-"
    # elsif diff > diff_pre
    #   print "+"
    # if diff == diff_pre
    #   print "="
    #   f = File.new("data_vector","w")
    #   binding.pry
    # end
    # diff_pre = diff

    if diff < $EPS
      # binding.pry
      return conceptSet
    end
    if iter >= 100
      print "iter >= 100"
      # binding.pry
      return conceptSet
    end
    iter += 1
    uijSet = uijSet_next.clone
  end
  binding.pry
  return conceptSet
end

def dis ai_t,center
  ai_keys = ai_t.keys
  c_keys = center.keys
  all_keys = ai_keys | c_keys
  Math.sqrt( all_keys.inject(0){ |sum, key| sum + (has_k?(ai_t,key) - has_k?(center,key))**2 } )
end

def has_k?(tags,key)
  return tags.has_key?(key) ? tags[key] : 0
end