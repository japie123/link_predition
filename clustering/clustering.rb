puts 'clustering'
load "#{$PRE}clustering/k_means.rb"
load "#{$PRE}clustering/fuzzy.rb"
KM, HI = 0,1
# decide how many group initial
def clustering(cluster_type, articleSet, addtion="",k)
  conceptSet = []
  case cluster_type
  when "K-means"
    print "\nK-means clustering "

    conceptSet = k_means articleSet, $KNUMBER, $DELTA
    conceptSet.each do |c|
        c.articles.each do |a|
          a.concepts = {}
          a.concepts[c.id] = 1.0
        end
      end
  when "K-means-2"
    # K-means
    # if File.exists?($NEW_CLUSTERING_CACHE+addtion)
    #   print "\nloading clustering cache :",$NEW_CLUSTERING_CACHE+addtion,"\n"
    #   conceptSet = YAML.load_file($NEW_CLUSTERING_CACHE+addtion)
    # else
    prev_pv = 0.0
    pre_diff = 1.0
    thre = k
    while 1
      print "\nk:",k,"\n"
      if k < 2 
        thre += 1
        next
      end 
      conceptSet = k_means articleSet, k, $DELTA
      
      
      conceptSet.each do |c|
        c.articles.each do |a|
          a.concepts = {}
          a.concepts[c.id] = 1.0
        end
      end
      if k > articleSet.size
        return conceptSet
      end
      pv = getPV conceptSet,articleSet.size
      print "\npv:",pv," "
      diff = pv-prev_pv
      print "\ndiff:",diff," "
      if diff >1 or pv > 1
        binding
      end
      if diff < pre_diff && k > thre
        print "\nfinal, k = ", k
        return conceptSet
      else
        prev_pv = pv
        pre_diff = diff
      end
      k+=1
    end
    #   f = File.new($NEW_CLUSTERING_CACHE+addtion,"w")
    #   f.puts conceptSet.to_yaml
    #   f.close
    # end
  when HI
    # Hierachical
  when "fuzzy"
    print "\nfuzzy clustering "
    conceptSet = fuzzy articleSet, $KNUMBER, $DELTA

  else
  end

  return conceptSet
end

def getPV conceptSet,arsize
  avgSet = []
  sumSet = []
  numSet = []
  conceptSet.each do |c|
    numSet << c.articles.length
    avg = {}
    c.articles.each do |a|
      a.tags.each do |t|
        avg[t[0]] = avg.has_key?(t[0]) ? avg[t[0]] + t[1] : t[1]
      end
    end
    avg.keys.each do |k|
      avg[k] = avg[k] / c.articles.length.to_f
    end 
    avgSet << avg
    
  end

  tavg = {}
  n = conceptSet.size
  (0...n).each do |i|
    av = avgSet[i]
    av.keys.each do |k|
      tavg[k] = tavg.has_key?(k) ? tavg[k] + numSet[i]*av[k] : numSet[i]*av[k]
    end
  end

  tavg.each do |h|
    tavg[h[0]] /= arsize
  end

  conceptSet.each do |c|
    sum = 0.0
    c.articles.each do |a|
      all_keys = tavg.keys
      sum += all_keys.inject(0){ |sum, key| sum + (pv_has_k?(a.tags,key) - tavg[key])**2 }
    end
    sumSet << sum/c.articles.length.to_f
  end

  bet = 0.0 # between
  within = 0.0 # within
  (0...n).each do |i|
    bet += (numSet[i]-1)*diss(avgSet[i],tavg)
    within += numSet[i]*sumSet[i]
  end
  if n>1
    bet /= (n-1)
  end
  
  if within == 0 
    return 0.0
  end

  return bet/within
end

def pv_has_k?a,key
  (a.has_key?(key) ? a[key] : 0)
end

def diss v1,v2
  all_keys = v1.keys | v2.keys
  all_keys.inject(0){ |sum, key| sum + (pv_has_k?(v1,key) - pv_has_k?(v2,key))**2 }
end