
def iterative conceptSet,features,articleSet,userSet
  print "\n total concept size: ",conceptSet.size
  print "\n total features size: ",features.size
  print "\n total articleSet size: ",articleSet.size
  iteration = 0
  Dir.mkdir("#{$PRE}cache/m2_c_split") unless File.directory?("#{$PRE}cache/m2_c_split")
  Dir.mkdir("#{$PRE}cache/m2_c_split/#{ARGV[0]}") unless File.directory?("#{$PRE}cache/m2_c_split/#{ARGV[0]}")
  Dir.mkdir("#{$PRE}cache/#{ARGV[0]}") unless File.directory?("#{$PRE}cache/#{ARGV[0]}")
  fset = []

  while true
    if File.exist?("#{$PRE}cache/#{ARGV[0]}/iter#{iteration}_features") and not File.exist?("#{$PRE}cache/#{ARGV[0]}/iter#{iteration+1}_features")
      print "\nload",iteration," iteration file..."
      conceptSet = YAML.load_file("#{$PRE}cache/#{ARGV[0]}/iter#{iteration}_conceptSet")
      userSet = YAML.load_file("#{$PRE}cache/#{ARGV[0]}/userSet_iter#{iteration}")
      fset = YAML.load_file("#{$PRE}cache/#{ARGV[0]}/iter#{iteration}_features")
      articleSet = YAML.load_file("#{$PRE}cache/#{ARGV[0]}/iter#{iteration}_articleSet")
      iteration += 1
    elsif File.exist?("#{$PRE}cache/#{ARGV[0]}/iter#{iteration}_features")
      print "\nskip ",iteration," iteration: "
      iteration += 1
      next
    end

    puts "\n\n",Time.now
    print "\n",iteration," iteration: "
    Dir.mkdir("#{$PRE}cache/#{ARGV[0]}/iter#{iteration}") unless File.directory?("#{$PRE}cache/#{ARGV[0]}/iter#{iteration}")

    print "\niter decide_user_concept..."
    userSet = decide_user_concept(conceptSet, userSet, iteration)
    print "\nrejudge_concept..."  
    c_split = []
    c_merge, c_split = rejudge_concept(conceptSet,userSet,iteration.to_s)
    # binding.pry
    if converge(c_merge, c_split)
      clusterPredict ARGV[0],userSet,conceptSet,fset,ARGV[0]+"_iter#{iteration}"
      print "\n\nconverge!!"
      break;
    end
    # fork do 
      # print "\nwrite out c_split: "
      # c_split.each do |c_s|
      #   print "."
      #   # next if File.exist? "cache/m2_c_split/#{ARGV[0]}/iter#{iteration}_#{c_s[0]}"
      #   f = File.new("#{$PRE}cache/m2_c_split/#{ARGV[0]}/iter#{iteration}_#{c_s[0]}","w")
      #   f.puts c_s[0]
      #   f.puts c_s[1]
      #   f.puts c_s[2].articles.size
      #   f.puts c_s[3].articles.size
      #   f.puts c_s.to_yaml
      #   f.close
      # end
    # end

    # generate keywords
    # regenerate feature every time
    $REGENERATE = false
    if $REGENERATE
      articleSet.each do |a|
        a.tags = {}
      end
    end
    if articleSet.empty? or conceptSet.empty?
      binding.pry
    end 
    articleSet,conceptSet,fset = adjust_article_tags(c_merge, c_split, articleSet, conceptSet, iteration, fset)
    if articleSet.empty? or conceptSet.empty? 
      binding.pry
    end 
    
    # fork do
      puts Time.now
      print "write out cache features_sel_iter#{iteration} and all_articles_iter#{iteration}..."
      f = File.new("#{$PRE}cache/#{ARGV[0]}/iter#{iteration}_features","w")
      f.puts fset.to_yaml
      f.close
      f = File.new("#{$PRE}cache/#{ARGV[0]}/iter#{iteration}_articleSet","w")
      f.puts articleSet.to_yaml
      f.close
      f = File.new("#{$PRE}cache/#{ARGV[0]}/iter#{iteration}_conceptSet","w")
      f.puts conceptSet.to_yaml
      f.close
      print "write out cache userSet_iter#{iteration}..."
      f = File.new("#{$PRE}cache/#{ARGV[0]}/userSet_iter#{iteration}","w")
      f.puts userSet.to_yaml
      f.close
    # end
    # binding.pry
    carr = []
    conceptSet.each do |cc|
      c = Concept.new(cc.id,cc.center)
      c.articles.replace cc.articles
      carr << c
    end
    # fork do
     clusterPredict ARGV[0],userSet,carr,fset,ARGV[0]+"_iter#{iteration}" 
    # end
    iteration +=1
  end
end