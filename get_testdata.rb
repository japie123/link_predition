require 'mysql2'
require 'yaml'
require 'pry'
require 'nokogiri'
load 'node.rb'
load 'construct_graph/setArticleVectors.rb'
# load 'construct_graph/construct_graph.rb'
# $idf = YAML::load_file 'idf_5.yml'

a_old_id =[]
u_old_id =[]
if not File.exists?("cache/train_articles_id")
  print "\nload userSet_old..."
  userSet_old = YAML.load_file("cache/m2_userSet_datafalsefalsefalse0")
  print "\nload articleSet_old..."  
  articleSet_old = YAML.load_file("cache/m2_articleSet_datafalsefalsefalse0")

  articleSet_old.each {|a_old| a_old_id << a_old.id}
  userSet_old.each {|u_old| u_old_id << u_old.id}

  f=File.new("cache/train_articles_id","w")
  f.puts a_old_id.to_yaml
  f.close
  f=File.new("cache/train_user_id","w")
  f.puts u_old_id.to_yaml
  f.close
else
  print "\nload a_old_id & u_old_id..."
  a_old_id = YAML.load_file("cache/train_articles_id")
  u_old_id = YAML.load_file("cache/train_user_id")
end

# 閱讀紀錄
hits_client = Mysql2::Client.new(:host => "localhost", :username => "root", :password => "1235813", :database => "hits")
tagging_client = Mysql2::Client.new(:host => "localhost", :username => "root", :password => "1235813", :database => "taggings")
articles_client =  Mysql2::Client.new(:host => "localhost", :username => "root", :password => "1235813", :database => "new_articles")

result = hits_client.query("select * from hits where created_at > '2013-10-07 09:07:57' and action_name = 'article' and user_id <> 'nil' and param_id <> 'nil';")
userSet_test = []
article_read = []
hit_hash = {}
print "\nresult.size: ",result.size
result.each_with_index do |h, index|
  if index%50000 == 0 then printf "\n%d ",index end
  if index%500 == 0 then printf "." end
  # new user is not needed
  next if not u_old_id.include?h['user_id']

  # record ids of articles hit 
  article_read << h["param_id"]

  # record user read article ids
  if hit_hash.has_key?(h['user_id'])
    hit_hash[h['user_id']].push(h['param_id'])
  else
    hit_hash[h['user_id']] = [h['param_id']]
  end
end
# f = File.new("cache/getTestdata_tmp_hit_hash")
# f.puts hit_hash.to_yaml
# f.close
# set new articles...
puts "select all tag relation..."
puts "\n",Time.now

# only new articles need to be selected and tagged
result = tagging_client.query("select * from taggings where created_at > '2013-10-07 05:46:53' and taggable_type = 'Article' and taggable_id <> 'nil' and tag_id <> 'nil';")
new_articleSet = []
print "\nresult.size: ",result.size
hash = {}
result.each do |x|
  next if x['tag_id']>478
  if hash.has_key?(x['taggable_id'])
    hash[x['taggable_id']].push(x['tag_id'])
  else
    hash[x['taggable_id']] = [x['tag_id']]
  end
end

print "\n creat new articles..."
print "hash.size: ",hash.size
hash.each_key do |key|
  # article not read from any user is not needed
  next if not article_read.include? key
  
  a = Article.new(key);
  for j in 0..hash[key].count-1
    a.concepts[hash[key][j]] = 1.0;
  end
  article = articles_client.query("select * from articles where id = #{key}").first
  if not article.nil?
    a.title = article['title']
    a.content = article['content']
    new_articleSet.push(a)

    # construct vectors of each article 
    setArticleVector(a)
    # setArticelVector_tf(a)
  end
end
print "\nnew_articleSet.size: ",new_articleSet.size
print "\nwrite out new articles"
f = File.new("cache/new_articleSet","w")
f.puts new_articleSet.to_yaml
f.close
puts "\n",Time.now

# only new articleIDs
# puts "link article and users ..."
# # {user_id => [param_id, param_id_2, ...]}
# printf "hit_hash.size: %d",hit_hash.size
# index = 0
# userSet = []
# a_exclu =a_old_id[100..-1]
# hit_hash.each_key do |key|
#   if index%1000 == 0 then printf "\n%d ",index end
#   if index%10 == 0 then printf "*" end
#   index += 1
#   u = User.new(key)
#   for j in 0..hit_hash[key].count-1
#     aID = hit_hash[key][j]
#     next if a_exclu.include? aID    
#     u.articleIDs << aID
#   end
#   u.articleIDs.uniq!
#   userSet.push(u)
# end
# print "\nuserSet.size: ",userSet.size

# print "\nwrite out new reading a100..."
# f = File.new("cache/new_reading_a100","w") # obj User with articleIDs.
# f.puts userSet.to_yaml
# f.close

