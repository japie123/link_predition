
def gen0 conceptSet, all_articles
  features_sel = []
  conceptSet_ori = conceptSet.clone
  
  # conceptSet = []
  # icf
  # icfSet = construct_icf(conceptSet_ori)
  # print "\nd.size/2:",d.size/2
  conceptSet_ori.each_with_index do |c,i|
    puts "\n\n",Time.now
    print i," concept "
    # next if i < d.size/3-1
    # if File.exist?("cache/gen0_feature/2c#{i}_features")
    #   print "\nload cache features_sel_2c#{i}..."
    #   features_sel = YAML.load_file("cache/gen0_feature/2c#{i}_features")
    #   print "\nload cache all_articles_2c#{i}..."
    #   all_articles = YAML.load_file("cache/gen0_feature/2c#{i}_articleSet")
    #   print "\nload cache concepts_2c#{i}..."
    #   conceptSet = YAML.load_file("cache/gen0_feature/2c#{i}_conceptSet")
    # else
      # construct two Group object
      g1 = Group.new(c.articles.last)
      c.articles.each do |a|
        g1.articles << a
      end
      g1.articles.pop
      g1.articles.uniq!
      # contsturct g.articles of all other concepts
      other_c = conceptSet_ori.clone
      other_c.delete(c)
      g2 = Group.new(other_c.last.articles.last)
      other_c.each do |c|
        c.articles.each do |a|
          g2.articles << a
        end
      end
      g2.articles.pop
      g2.articles.uniq!
      
      features = get_feature [i,0,g1,g2]
      print "\nfeatures.size: ",features.size
      
      correct = -1
      while correct < 0.9
        # correct = -1
        f_sel = nil
        del_f = nil
        min_dis = Float::INFINITY
        
        conceptSet_end = nil
        f_pre = ""
        features.each do |f_hash|

          f = f_hash[0]
          print "\nfeature: ",f
          # reset tags 
          articleSet_c = []
          all_articles.each_with_index do |a,ind|
            a.tags.delete f_pre
            # if ind%5000 == 0 then print ind," " end
            a_vector = YAML.load_file("#{$PRE}articles_vector/#{a.id}")
            if a_vector.has_key? f
              a.tags[f] = a_vector[f]              
            else
              a.tags[f] = 0.0
            end
            articleSet_c << a
          end
          
          print "\nclustering..."
          print "\narticleSet_c.size: ",articleSet_c.size
          conceptSet_new = clustering($CLUSERTING_TYPE, articleSet_c,"_m2_#{i}_#{f}",12)
          # compare conceptSet
          intersect = distance(conceptSet_new,conceptSet_ori)
          # binding.pry
          intersect /= all_articles.size
          print "\nconceptSet distance...intersect: ",intersect

          if intersect > correct
            correct = intersect
            print "\ncorrect: ",correct
            f_sel = f
            del_f = f_hash
            conceptSet_end = conceptSet_new.clone
          end
          f_pre = f
        end
        all_articles.each_with_index do |a,ind|
          a.tags.delete f_pre
        end
        puts "\ndone sel feature"
        puts "f_sel: ",f_sel
        # select feature
        if f_sel.nil?
          print "x"
          break
        end
        features_sel << f_sel
        features.delete(del_f)
        
        if conceptSet_end.nil?
          binding.pry
        end
        conceptSet = conceptSet_end.clone
        all_articles.each do |a|
          a_vector = YAML.load_file("#{$PRE}articles_vector/#{a.id}")
          # icfSet[a.id][f]
          if a_vector.has_key? f_sel
            print "!"
            a.tags[f_sel] = a_vector[f_sel]
          else
            a.tags[f_sel] = 0
          end
        end

        print "\ncorrect: ",correct,"\n"
        if features.empty?
          print "\nfeatures.empty!"
          break
        end
      end
    #   print "write out cache features_sel_2c#{i} and all_articles_2c#{i}..."
    #   f = File.new("cache/gen0_feature/2c#{i}_features","w")
    #   f.puts features_sel.to_yaml
    #   f.close
    #   f = File.new("cache/gen0_feature/2c#{i}_articleSet","w")
    #   f.puts all_articles.to_yaml
    #   f.close
    #   f = File.new("cache/gen0_feature/2c#{i}_conceptSet","w")
    #   f.puts conceptSet.to_yaml
    #   f.close
      
    # end
  end
  return features_sel, all_articles, conceptSet
end