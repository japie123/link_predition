load 'node.rb'
require 'mysql2'
require 'yaml'
require 'pry'
client = Mysql2::Client.new(:host => "localhost", :username => "root", :password => "1235813", :database => "link_prediction")
puts "select hits ..."
result = client.query("select * from hits where action_name = 'article' and user_id <> 'nil' and param_id <> 'nil';")
print "ruby bigram.rb outputname randomsize"

articleSet = YAML.load_file("cache/m2_articleSet_datafalsefalsefalse0")
articleSet_c = Set.new
randomsize = ARGV[1].to_i
while articleSet_c.size < randomsize
  print "."
  articleSet_c << rand(articleSet.size)
end

all_id = []
articleSet_c.each do |a|
  all_id << a
end

article_read = []
hit_hash = {}
puts "result.size", result.size
result.each_with_index do |x, index|
  if index%50000 == 0 then printf "\n%d ",index end
  if index%500 == 0 then printf "." end
    next if not all_id.include? x['param_id']
  # record ids of articles hit 
  article_read << x["param_id"]

  # record user read article ids
  if hit_hash.has_key?(x['user_id'])
    hit_hash[x['user_id']].push(x['param_id'])
  else
    hit_hash[x['user_id']] = [x['param_id']]
  end
end

# statistic the total prob
puts "hit_hash.size", hit_hash.size
prob = {}
hit_hash.each_with_index do |u,index|
  if index%10000 == 0 then printf "\n%d ",index end
  if index%100 == 0 then printf "." end
  article_seq = u[1]
  prev = nil
  print ">",article_seq.size
  article_seq.each do |a|
    # next if prev.nil?
    prob[prev] ||= []
    prob[prev] << a
    prev = a
  end
end
# transition from count to prob
print "\ntransition from count to prob...\n"
puts "prob.size", prob.size
# binding.pry
set = {}
prob.each_with_index do |a,index|
  if index%10000 == 0 then printf "\n%d ",index end
  if index%100 == 0 then printf "." end
  count = {}
  probability = {}
  all = a[1].size.to_f

  a[1].each do |nexta|
    count[nexta] ||= 0
    count[nexta] += 1
  end
  not_repeated = a[1].uniq
  not_repeated.each do |nexta|
    probability[nexta] = count[nexta]/all
  end
  set[a[0]] = probability.sort_by {|k,v| -1*v}
end

print "\nwrite out probability"
# f=File.new("bigram_prob","w")
# f.puts set.to_yaml
# f.close
# binding.pry
# recommend the top 100 pro??
userSet = YAML.load_file("cache/m2_userSet_datafalsefalsefalse0")
userSet_predict = []
print "\nuserSet.size: ",userSet.size
userSet.each_with_index do |u,i|
  last_read = u.articleIDs.last
  prob = set[last_read]
  u_new = User.new(u.id)
  if not prob.nil?
    prob.each do |p|
      next if u.articleIDs.include? p[0]
      u_new.articleIDs << p[0]
    end
  end
  if u_new.articleIDs==0
    print "? "
  end
  userSet_predict << u_new
end

f= File.new("cache/userSet_predict_bigram#{ARGV[0]}","w")
f.puts userSet_predict.to_yaml
f.close


