print "$VERSION $GRAPHCASE $ONLYINTERESTS " 
print "$ONLYTOPICS $LIMITDATA $POSTAVERAGE "
print "$COMMON_USER_SUM [$TAGNUMBER]\n"
# if ARGV.size < 7
#   puts "[ERROR] ARGV.size < 7!"
#   print "$VERSION $GRAPHCASE $ONLYINTERESTS " 
#   print "$ONLYTOPICS $LIMITDATA $POSTAVERAGE "
#   print "$COMMON_USER_SUM [$TAGNUMBER]"
#   raise ArgumentError
# end

$DICTIONARY = "womany_dictionary_5.yml"
Dir.mkdir("#{$PRE}cache") unless File.directory?("#{$PRE}cache")
Dir.mkdir("#{$PRE}log") unless File.directory?("#{$PRE}log")
# $-f = File.new("#{$PRE}log/debug_#{(Time.now.day).to_s + (Time.now.hour).to_s}.txt",'w')
$DEBUG = false
$DEBUG_more = false
$VERSION = ARGV[0]
$SKIP = false
$B4MERGE = false
# $VERSION = AGV[0]
$cache = File.new("#{$PRE}log/cache_$VERSION","w")

## construct_graph
$GRAPHCASE = ""
$TAG_CONTEXT = 'interests'
# $USERNUMBER = 48395
$USERNUMBER = 24272
$ARTICLENUMBER = 3404
$TAGNUMBER = 0
$TESTFORMINCUT = false
$TAGDISCRETE = true

$ONLYINTERESTS = ARGV[2]
$ONLYTOPICS = ARGV[3]
$LIMITDATA = ARGV[4]
$POSTAVERAGE = "" # after
# $POSTAVERAGE = false
# $PREAVERAGE = true
$COMMONAVERAGE = true

## clustering
$CLUSERTING_TYPE = ARGV[2] # fuzzy /K-means


if ARGV[1] == "data"
  $KNUMBER = 100
elsif ARGV[1] == "random"
  $KNUMBER = 100
  $TAGNUMBER = ARGV[7]
  $ARTICLE_NUMBER_OF_ONEUSER = 45
end
$KNUMBER = 7
$DELTA = 0.001 # center does not need to be changed, resolution


$TAGS = $GRAPHCASE.to_s + $ONLYINTERESTS.to_s + $ONLYTOPICS.to_s + $LIMITDATA.to_s + $TAGNUMBER.to_s
$TAGS_AVR = $GRAPHCASE.to_s + $ONLYINTERESTS.to_s + $ONLYTOPICS.to_s + $LIMITDATA.to_s + $POSTAVERAGE.to_s

## redudge_concept
$CUT_TYPE = ARGV[3] # excluKarger/greedy
# $SPLIT_THRES = 498.916
$SPLIT_THRES = 15.916
$COMMON_USER_SUM = ARGV[6]
COMMON_USER_SUM = true
$COMMON_USER_REVERSE = false
$WEIGHTLEVLE = 1000

# article weight - for maximum cut
# if $CUT_TYPE == "exclusive_weight"
  $EXCLUSIVEWEIGHT = true
# end

## Karger's algo
$REPEAT = 10
$SAMPLE_TIMES = 1000
$SPLITFILE = "#{$PRE}split/final/split_#{$VERSION}"

# adjust_article_tags
# $CHI_PATH = "chi/#{$VERSION}"
$CHI_PATH = "#{$PRE}chi/#{$VERSION}"
# `mkdir split/groupb4Merge_#{$VERSION}_#{$TAGS_AVR}`

# cache names
## construct_graph
# $LINK_ARTICLE_USER_NAME = "cache/userSet_articleSet_cache_#{$TAGS}"
$LINK_ARTICLE_USER_NAME = "#{$PRE}cache/userSet_articleSet_cache_new_interest"
# $LINK_ARTICLE_USER_NAME = "cache/tmp"
$LINK_ARTICLE_USER_NAME_LIMIT = "#{$PRE}cache/userSet_articleSet_cache_limit_#{$TAGS}"
## clustering 
$CLUSTERING_CACHE = "#{$PRE}cache/clustering_#{$TAGS}"
Dir.mkdir("#{$PRE}cache/iter_clusters/") unless File.exists?("#{$PRE}cache/iter_clusters/")
$NEW_CLUSTERING_CACHE = "#{$PRE}cache/iter_clusters/"
## decide_user_concept
$USER_CONCEPT = "#{$PRE}cache/user_concept_#{ARGV[0]}"

# print out configure
# $f.puts "\nconfigure:"
# $f.print "\n$VERSION: ",$VERSION
# $f.print "\n$KNUMBER: ",$KNUMBER
# $f.print "\n$ONLYINTERESTS: ",$ONLYINTERESTS
# $f.print "\n$ONLYTOPICS: ",$ONLYTOPICS
# $f.print "\n$LIMITDATA: ",$LIMITDATA
# $f.print "\n$MINCUT_TYPE: ",$MINCUT_TYPE
# $f.print "\n$SPLIT_THRES: ",$SPLIT_THRES
# $f.print "\n$CHI_PATH: ",$CHI_PATH
# $f.print "\n$TAGDISCRETE: ",$TAGDISCRETE

puts "\nconfigure:"
print "\n$VERSION: ",$VERSION
print "\n$SKIP: ",$SKIP
print "\n$KNUMBER: ",$KNUMBER
print "\n$ONLYINTERESTS: ",$ONLYINTERESTS
print "\n$ONLYTOPICS: ",$ONLYTOPICS
print "\n$LIMITDATA: ",$LIMITDATA
print "\n$MINCUT_TYPE: ",$MINCUT_TYPE
print "\n$SPLIT_THRES: ",$SPLIT_THRES
print "\n$CHI_PATH: ",$CHI_PATH
print "\n$POSTAVERAGE: ",$POSTAVERAGE
print "\n$PREAVERAGE: ",$PREAVERAGE
print "\n$COMMON_USER_SUM: ",$COMMON_USER_SUM
print "\n$COMMON_USER_REVERSE: ",$COMMON_USER_REVERSE
print "\n$WEIGHTLEVLE: ",$WEIGHTLEVLE
print "\n$TAGDISCRETE: ",$TAGDISCRETE
print "\n$B4MERGE: ",$B4MERGE
print "\n$EXCLUSIVEWEIGHT: ",$EXCLUSIVEWEIGHT
print "\n$TAG_CONTEXT: ",$TAG_CONTEXT

if ARGV[1] == "random"
  print "\nrandom:"
  print "\n$USERNUMBER: ",$USERNUMBER
  print "\n$TAGNUMBER: ",$TAGNUMBER 
  print "\n$TESTFORMINCUT: ",$TESTFORMINCUT 
  print "\n$TAGDISCRETE: ",$TAGDISCRETE 
  print "\n$ARTICLE_NUMBER_OF_ONEUSER: ",$ARTICLE_NUMBER_OF_ONEUSER
end 
puts ""
