require 'json'
require 'yaml'
# require 'mysql2'

# @client = Mysql2::Client.new(:host => "localhost", :username => "root", :password => "1235813", :database => "link_prediction")
TMP_FILE = "#{$PRE}log/tmp.txt"

$FEATURE_OUTPUT = "#{$CHI_PATH}/#{$VERSION}_Feature_selected_"


def adjust_article_tags(c_merge, c_split, aSet, cSet, iter, fset)
  printf "\nadjust_article_tags"
  print "\nc_split.size: ",c_split.size
  k = c_split.size + cSet.size
  # get feature via chi2
  features_sel = []
  conceptSet = cSet
  if conceptSet.nil?
    binding.pry
  end
  articleSet = aSet
  c_split.each_with_index do |c_s,i|
    printf "\n\n%d c_split: ...\n",i
    features = get_feature c_s
    if features.nil?
      next
    end
    # choose features which could reserve 90% original concept
    correct = -1.0
    while correct < 0.9
      correct = -1.0
      f_sel = ''
      del_f = ''
      min_dis = Float::INFINITY
      
      print "features.size: ",features.size
      conceptSet_end = nil
      f_pre = ""
      features.each do |f_hash|

        f = f_hash[0]
        print "\nfeatures: ",f

        articleSet_c = []
        if File.exist? "#{$PRE}cache/#{ARGV[0]}/iter#{iter}/#{i}_#{f}"
          print "\nload cache #{ARGV[0]}/iter#{iter}/#{i}_#{f}..."
          articleSet_c = YAML.load_file("#{$PRE}cache/#{ARGV[0]}/iter#{iter}/#{i}_#{f}")
        else
          print "all_articles.size: ",articleSet.size,"\n"
          articleSet.each_with_index do |a,ind|
            a.tags.delete f_pre
            if ind%5000 == 0 then print ind," " end
            a_vector = YAML.load_file("#{$PRE}articles_vector/#{a.id}")
            aa = Article.new(a.id,a.tags)
            aa.users.replace a.users
            a.concepts.replace a.concepts
            if a_vector.has_key? f
              # print a_vector[f]," "
              aa.tags[f] = a_vector[f]
            # else
            #   a.tags[f] = 0.0
            end
            articleSet_c << aa
          end
          print "write out cache #{ARGV[0]}/iter#{iter}/#{i}_#{f}..."
          file = File.new("#{$PRE}cache/#{ARGV[0]}/iter#{iter}/#{i}_#{f}","w")
          file.puts articleSet_c.to_yaml
          file.close
        end

        print "\nclustering..."
        print "\narticleSet_c.size: ",articleSet_c.size
        conceptSet_new = clustering($CLUSERTING_TYPE, articleSet_c,"iter#{iter}_#{i}_#{f}",k)
        # compare conceptSet
        print "\nconceptSet distance..."
        intersect = distance(conceptSet_new,conceptSet)
        intersect /= articleSet.size

        if intersect > correct
          correct = intersect
          print "\ncorrect: ",correct
          f_sel = f
          del_f = f_hash
          conceptSet_end = conceptSet_new
        end
        f_pre = f
      end
      articleSet.each_with_index do |a,ind|
        a.tags.delete f_pre
      end
      puts "\ndone sel feature"
      puts "f_sel: ",f_sel
      # select feature
      features_sel << f_sel
      features.delete(del_f)
      if conceptSet_end.nil?
        binding.pry
      end
      conceptSet = conceptSet_end.clone
      articleSet.each do |a|
        a_vector = YAML.load_file("#{$PRE}articles_vector/#{a.id}")
        # icfSet[a.id][f]
        if a_vector.has_key? f_sel
          print "!"
          a.tags[f_sel] = a_vector[f_sel]
        else
          a.tags[f_sel] = 0
        end
      end

      print "\ncorrect: ",correct,"\n"
      if features.empty?
        print "\nfeatures.empty!"
        break
      end
      puts Time.now
    end
  end
  fset = fset + features_sel
  # `rm -irf #{$PRE}cache/#{ARGV[0]}/iter#{iter}/*`
  return articleSet,conceptSet,fset
end
