puts 'decide_user_concept'
def decide_user_concept(conceptSet, userSet, iter)
  puts "\n*** decide_user_concept ***\n"

  # caculate how many users connected to this concept
  # c.number_of_users is decided
  
  puts "Timmmmmme: ",Time.now
  # if File.exists?($USER_CONCEPT+iter.to_s)
  #   puts "loading user-concept cache ..."
  #   userSet = YAML.load_file($USER_CONCEPT+iter.to_s)
  # else
    puts "calculate_concept_users ..."
    if userSet.empty?
      binding.pry
    end
    calculate_concept_users(conceptSet, userSet)
    
    printf "\ncalculate article weight ... \n"
    puts "Timmmmmme: ",Time.now
  
    # calculate weight of each article
    #                 (number of users see article_a )
    #  weight_a =  ------------------------------------
    #                 (total user belong this concept)
    a_w = []
    count_a = 0
    conceptSet_0 = conceptSet.clone

    conceptSet.each_with_index do |c, index|
      c.total_articles_weight = 0;
      cId = c.id
      # print c.articles.size,"-"
      c.articles.each do |a|
        # printf "|"
        a.weight[cId] = a.users.length() / c.number_of_users.to_f
        c.total_articles_weight += a.weight[cId]
        a_w << a
        count_a+=1
      end
    end

    printf "\nuser-concept vector ... "
    puts "\nTimmmmmme: ",Time.now
    # user-concept vector 
    us = userSet.size
    cs = conceptSet.size
    print "\nuserSet.size*conceptSet.size: ",us*cs
    print "\nuserSet.size: "
    puts us
    puts "\nconceptSet.size: "
    puts cs
    conceptSet_1 = conceptSet.clone
    print "\nset user-concept vector.."
    userSet.each_with_index do |u, index|
      u.conceptSet = {}
      if index%10000==0 
        puts "",Time.now
        printf "%d ",index         
      end
      if index%100==0 then printf "<" end
      conceptSet.each_with_index do |c,i|
        # if i%5000==0 then printf "|" end
        see_article_weight = 0
        cId = c.id
        c.articles.each do |a|
          # printf "|"
          if u.articleIDs.include? a.id
            if a.weight[cId].nil?
              binding.pry
            end
            see_article_weight += a.weight[cId]
          end
        end
        u.conceptSet[c.id] = see_article_weight / c.total_articles_weight 
      end
    end
    # puts "\nTimmmmmme: ",Time.now
    # f = File.new($USER_CONCEPT+iter.to_s,"w")
    # f.puts userSet.to_yaml
    # f.close
  # end

  return userSet
end

def calculate_concept_users(conceptSet, userSet)
  print "conceptSet.size: ",conceptSet.size,"\n"
  print "userSet.size: ",userSet.size,"\n"
  # total users of rach concept
  conceptSet.each_with_index do |c, index|    
    users = []
    c.articles.each do |a|
      print "."
      users = a.users | users
    end
    users.uniq!
    print "\n"
    c.number_of_users = users.size
  end
end
