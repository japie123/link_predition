
$PRE = ARGV[1]
load "#{$PRE}node.rb"
require 'yaml'
require 'set'
require 'pry'
load "#{$PRE}accuracy.rb"
load "#{$PRE}hitratio.rb"
load "#{$PRE}roc.rb"
load "#{$PRE}methods.rb"
load "#{$PRE}construct_graph/setArticleVectors.rb"
  puts "\n",Time.now

print "\nload userSet..."
userSet = YAML.load_file("#{$PRE}cache/m2_userSet_datafalsefalsefalse0")
articleSet = YAML.load_file("#{$PRE}cache/m2_articleSet_datafalsefalsefalse0")
userSet_predict = []
profileSet = {}
userSet = userSet[0...100]
userSet.each_with_index do |u,i|
  if i%1000 == 0 then print "\n",Time.now,"\n",i," " end
  print "\n",i," "

  # get profile
  profile = {}
  print u.articleIDs.size," "
  puts "\n",Time.now
  print "build profile..."
  
  # n = u.articleIDs.size
  u.articleIDs.each do |aid|
    print "*"
    a_vector = nil
    next if not File.exist?("#{$PRE}articles_vector/#{aid}")
    a_vector = YAML.load_file("#{$PRE}articles_vector/#{aid}")
    
    a_vector.each do |t|
      profile[t[0]] = profile.has_key?(t[0]) ? 0.8*profile[t[0]] + 0.2*t[1] : 0.2*t[1]
    end     
  end  
  profileSet[u.id] = profile
end

  # recommendation
  puts "\n",Time.now
  print "\nrecommendaion..\n"
  scoreSet = {}
  print "\nloadnew articleSet... "
  new_articleSet = YAML.load_file("#{$PRE}cache/new_articleSet")
  puts "\n",Time.now
  
  new_articleSet.each_with_index do |a,i|
    if i%100 == 0 then print "\n",Time.now,"\n",i," " end
    print "*"
    a_vector = YAML.load_file("#{$PRE}articles_vector/#{a.id}")
    userSet.each_with_index do |u,i|
      if i%10 == 0 then print "." end
      scoreSet[u.id] ||=  {}
      profile = profileSet[u.id]
      scoreSet[u.id][a.id] = dis(profile,a_vector)
    end
  end
  puts "\n",Time.now

  print "\nload articleSet"
  articleSet.each_with_index do |a,i|
    if i%100 == 0 then print i," " end
    a_vector = YAML.load_file("#{$PRE}articles_vector/#{a.id}")
    userSet.each_with_index do |u,i|    
      scoreSet[u.id] ||=  {}
      profile = profileSet[u.id]
      scoreSet[u.id][a.id] = dis(profile,a_vector)
    end
  end
  puts "\n",Time.now

  
  scoreSet.each do |s|
    uid = s[0]
    score = s[1]
    sort_article = score.sort_by {|k,v| -1*v}
    u_new = User.new(uid)
    u = userSet.find{|x| x.id == uid}
    sort_article.each do |a|
      next if u.articleIDs.include? a
      u_new.articleIDs << a[0]
    end
    userSet_predict << u
  end
  # binding.pry
  puts "\n",Time.now

print "\nload userSet_right..."
userSet_right = YAML.load_file("#{$PRE}cache/new_reading")
all_hit = 0
userSet_right.each do |u|
  all_hit += u.articleIDs.size
end
accuracy(userSet_predict, userSet_right, "content_#{ARGV[0]}", 100)

