puts 'node.rb'
$ARTICLE_L2DISTANCE, $ARTICLE_COSINE_DIST = 1, 0

class User
  # id just can be read
  attr_reader :id 
  # tags and concept can be read/write
  # usage example: u.concept[index] = weight
  attr_accessor :tags
  attr_accessor :conceptSet
  attr_accessor :articleIDs
  attr_accessor :newArticleIDs
  def initialize(id, tags = {})
    @id = id
    @tags = tags
    @conceptSet = {}
    @articleIDs = []
    @newArticleIDs = []
  end
end

class Article
  attr_accessor :id
  attr_accessor :tags
  attr_accessor :concepts # key:詞
  attr_accessor :keywords
  attr_accessor :users
  attr_accessor :belonged_concept
  attr_accessor :title
  attr_accessor :content
  attr_accessor :weight # key: concept id
  attr_accessor :link_articles # key: article obj
  attr_accessor :self_link
  attr_accessor :other_link
  attr_accessor :side
  def initialize(id,t={})
    @id = id
    @concepts = {}
    @keywords = {}
    @tags = t
    @users = []
    @belonged_concept = []
    @title = ''
    @content = ''
    @weight = {}
    @link_articles = {}
    @self_link = 0
    @other_link = 0
    @side = 0
  end
  def distance_to(center)
    all_keys = @tags.keys | (center).keys
    d = 0.0;
    if $ARTICLE_L2DISTANCE
      # L2 distance: squrt(sigma(x^2+y^2))
      d = Math.sqrt( all_keys.inject(0){ |sum, key| sum + (has_tag?(@tags,key) - has_tag?(center,key))**2 } )
    elsif $ARTICLE_COSINE_DIST
      # cosine similartiy
      d = @tags.keys.inject(0){ |sum, key| sum + @tag[key] * center[key] }
    end
    # puts 'd',d 
    return d 
  end
  def has_tag?(tags,key)
    # if tags.has_key?(key)
    #   print ' has key: value = ',tags[key]
    # else
    #   puts
    #   print '!!! donnot have key '
    #   puts 
    # end
    return tags.has_key?(key) ? tags[key] : 0
  end
  def keys
    @tags.keys
  end
  def add_link_article(n,a)
    @link_articles[a] = n
  end
end

class Concept
  attr_reader :id
  attr_accessor :tags
  attr_accessor :articles
  attr_accessor :center
  attr_accessor :total_articles_weight
  attr_accessor :number_of_users
  def initialize(id,c = nil)
    @id = id
    # puts 'id: ',id
    @tags = {}
    @articles = []
    @center = c
    @number_of_users = 0
    @total_articles_weight = 0
  end
  def include?(aid)
    articles.each do |a|
      if a.id == aid
        # print 'return true!!'
        return true
      end
    end
    false
  end
  def number_of_article
    # if @articles.length == 0
    #   puts 'zeroooo'
    #   puts 'center: ',center.tags
    # end
    @articles.length
  end
  def update_center(delta = 0.001)
    centerTags = {}

    @articles.each do |a|
      a.tags.each do |t|
        centerTags[t[0]] = centerTags.has_key?(t[0]) ? centerTags[t[0]] + t[1] :t[1]
      end
    end

    # average
    centerTags.keys.each do |k|
      centerTags[k] = centerTags[k] / @articles.length.to_f
    end 

    @center = centerTags    
  end
end

class Cluster < Concept
  attr_accessor :center # an Article object
  attr_reader :moved
  def initialize(id, center)
    @id = id
    @center = center
    # puts 'id:',@id
    # puts 'center:',@center
    # puts 'end'
    @number_of_users = 0
    
    @moved = true
    # puts 'center.tags',center.tags
  end
  def moved
    @moved
  end
  def addArticle(a)
    # puts a.tags
    @articles << a
  end
  def reset
    @articles = []
  end
  def ending
    # calculate each tag vector and average
    @articles.each do |a|
    end
  end
  def update_center(delta = 0.001)
    @moved = false
    centerTags = {}
    # puts
    # puts '=== update_center(delta = 0.001) ==='
    # puts 'articles.length:',articles.length
    # puts 'centerTags initial:',centerTags

    it = 0;
    @articles.each do |a|
      it = it + 1;
      a.tags.each do |t|
        centerTags[t[0]] = centerTags.has_key?(t[0]) ? centerTags[t[0]] + t[1] :t[1]
      end
    end
    # puts 'it: ', it
    # puts 
    # puts 'articles.length:',articles.length
    # centerTags.each do |t|
    #   printf '%d:%d ',t[0],t[1]*10000
    # end

    # average
    centerTags.keys.each do |k|
      centerTags[k] = centerTags[k] / @articles.length.to_f
      # printf '%d %d ; ',k,centerTags[k]*10000
    end 

    # centerTags.each do |t|
    #   printf '%d: %f -> ', t[1],t[1]/articles.length.to_f
    #   t[1] = t[1].to_f/articles.length
    #   printf '%d ; ', t[1]
    # end
    # puts
    # centerTags.each do |t|
    #   printf '%d:%d ',t[0],t[1]
    # end
    # puts articles.length
    # puts 'centerTags:'
    # print centerTags
    # puts
    unless Article.new(0,centerTags).distance_to(@center) < delta
      @center = centerTags
      @moved = true
    end
  end
end

class Group
  attr_accessor :link_groups # key:groupobj
  attr_accessor :articles
  def initialize a=nil
    @link_groups = {}
    @articles = []
    if not a.nil?
      @articles << a
    end
  end
  def link_group g,w
    links[g] = w
  end
end
