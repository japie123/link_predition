$PRE = ARGV[1]
load "#{$PRE}node.rb"
load "#{$PRE}accuracy.rb"
load "#{$PRE}hitratio.rb"
load "#{$PRE}roc.rb"
require 'yaml'
require 'pry'
require 'set'

articleSet = YAML.load_file("#{$PRE}cache/m2_articleSet_datafalsefalsefalse0")
# articleSet = articleSet[0...100]
# randomsize = ARGV[1].to_i
# articleSet_c = Set.new
# while articleSet_c.size < randomsize
#   print "."
#   articleSet_c << rand(articleSet.size)
# end

# all_id = []
# articleSet_c.each do |a|
#   all_id << a
# end

# predict the articles
userSet = YAML.load_file("#{$PRE}cache/m2_userSet_datafalsefalsefalse0")
userSet_predict = []
print "userSet.size: ",userSet.size
userSet.each_with_index do |u,i|
  similarity = {}
  if i%1000 == 0 then print "\n",i," " end
  if i%1 == 0 then print "." end
  # print "\n ",i," user_id: ",u.id,"\n"
  u1 = u.articleIDs.size.to_f
  userSet.each_with_index do |u_link,ind|
    # if ind%10000==0 then print " ",ind," " end
    next if u_link.id == u.id
    u2 = u_link.articleIDs.size
    intersect = u.articleIDs & u_link.articleIDs
    cos_article = intersect.size/(u1*u2)
    # print cos_article," "
    if cos_article > 0
      similarity[u_link] = cos_article
    end
  end
  # get nearest neighbor
  neighbor = similarity.sort_by {|k,v| -1*v}
  k = 30
  if neighbor.size < k then k = neighbor.size end
  if k < 30 then print "!" end
  collectArticle = {}
  for i in 0...k
    u_chosen = neighbor[i][0]
    u_chosen.articleIDs.each do |a|
      collectArticle[a] ||= 0
      collectArticle[a] += neighbor[i][1]
    end
  end
  article_rec = collectArticle.sort_by {|k,v| -1*v}
  u_new = User.new(u.id)
  for i in 0...article_rec.size
    # next if all_id.include?article_rec[i][0]
    next if u.articleIDs.include? article_rec[i][0]
    u_new.articleIDs << article_rec[i][0]
  end
  userSet_predict << u_new
end

# load 'accuracy.rb'
# # accuracy rate 
# userSet_right = YAML.load_file("cache/new_reading") 
accuracy(userSet_predict, userSet_right, "userbasedCF_#{ARGV[0]}", 100)
hitratio(userSet_predict, userSet_right, "userbasedCF_#{ARGV[0]}", all_hit)
roc(userSet_predict, userSet_right, "userbasedCF_#{ARGV[0]}")

f= File.new("#{$PRE}cache/userSet_predict_userbasedCF_#{ARGV[0]}","w")
f.puts userSet_predict.to_yaml
f.close