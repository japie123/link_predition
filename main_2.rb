require 'pry'
require 'set'
require 'yaml'
require 'mysql2'
require 'nokogiri'

puts 'main_2.rb'
load 'configure.rb'
load 'node.rb'
# load 'construct_graph/cognstruct_graph.rb'
# load 'construct_graph/construct_icf.rb'
load 'clustering/clustering.rb'
load 'decide_user_concept/decide_user_concept.rb'
load 'converge.rb'
load 'rejudge_concept/rejudge_concept.rb'
load 'adjust_article_tags/adjust_article_tags.rb'
load 'methods.rb'

# from existing article
# modify article form tags to concept
conceptSet = []
userSet = []
articleSet = []
articleSet_new = []
conceptSet_t = {}
if not File.exist?("cache/m2_conceptSet_datafalsefalsefalse0")
  puts "loading old full article-user cache ...:"
  puts "userSet_articleSet_cache_new_datafalsefalsefalse0"
  arr = YAML.load_file("cache/userSet_articleSet_cache_new_datafalsefalsefalse0")
  userSet = arr[0]
  articleSet = arr[1]
  # modify article form tags to concept
  articleSet.each_with_index do |a,ind|
    if ind % 100 == 0 then print ind," " end
    # break if ind== 1000
    a_new = Article.new(a.id)
    a_new.users = a.users
    a_new.title = a.title
    a_new.content = a.content
    a_new.link_articles = a.link_articles

    # from tags to concept...
    tags = a.tags
    tags.each_with_index do |t_hash,i|
      a_new.concepts[t_hash[0]] = t_hash[1]
      conceptSet_t[t_hash[0]] ||= [] 
      conceptSet_t[t_hash[0]] << a_new
    end
    articleSet_new << a_new
  end
  conceptSet_t.each_with_index do |c_t,i|
    if i % 100 == 0 then print i," " end  
    c = Concept.new(i)
    c.articles.replace c_t[1]
    centerTags = {}    
    c_t[1].each do |a|
      a.concepts.each do |t|
        centerTags[t[0]] = centerTags.has_key?(t[0]) ? centerTags[t[0]] + t[1] :t[1]
      end
    end
    centerTags.keys.each do |k|
      centerTags[k] = centerTags[k] / c.articles.size.to_f      
    end
    c.center = Article.new(0,centerTags)
    conceptSet << c
  end

  binding.pry
  # f=File.new("cache/m2_userSet_datafalsefalsefalse0","w")
  # f.puts userSet.to_yaml
  # f.close
  # f=File.new("cache/m2_articleSet_datafalsefalsefalse0","w")
  # f.puts articleSet.to_yaml
  # f.close
  f=File.new("cache/m2_conceptSet_datafalsefalsefalse0","w")
  f.puts conceptSet.to_yaml
  f.close
else
  puts "loading full article-user cache ...:"
  # puts "load userSet"
  # userSet = YAML.load_file("cache/m2_userSet_datafalsefalsefalse0")
  puts "load articleSEt..."
  articleSet = YAML.load_file("cache/m2_articleSet_datafalsefalsefalse0")
  puts "load conceptSet..."
  conceptSet = YAML.load_file("cache/m2_conceptSet_datafalsefalsefalse0")
end
# binding.pry
# ====================================
# generation 0: from conceptSet to extract features
features_sel = []
if File.exist?("cache/generation0_fel_#{ARGV[0]}")
  print "\nload cache generation0_fel_#{ARGV[0]}..."
  features_sel = YAML.load_file("cache/generation0_fel_#{ARGV[0]}")
else
  conceptSet_ori = conceptSet.clone
  all_articles = []
  conceptSet.each do |c|
    all_articles += c.articles
  end
  print "\nall_articles.size: ",all_articles.size
  all_articles.uniq!
  print "\nall_articles.size: ",all_articles.size

  # conceptSet = []
  # icf
  # icfSet = construct_icf(conceptSet_ori)
  d = Dir["cache/features_sel_9/*"]
  print "\nd.size/2:",d.size/2
  conceptSet_ori.each_with_index do |c,i|
    puts "\n\n",Time.now
    print i," concept "
    next if i < d.size/3-1
    if File.exist?("cache/features_sel_9/features_sel_2c#{i}")
      print "\nload cache features_sel_2c#{i}..."
      features_sel = YAML.load_file("cache/features_sel_9/features_sel_2c#{i}")
      print "\nload cache all_articles_2c#{i}..."
      all_articles = YAML.load_file("cache/features_sel_9/all_articles_2c#{i}")
      print "\nload cache concepts_2c#{i}..."
      conceptSet = YAML.load_file("cache/features_sel_9/conceptSet_2c#{i}")
    else
      if File.exist?("cache/ge0_features_#{ARGV[0]}_#{i}")
        print "\nload cache ge0_features_#{ARGV[0]}_#{i}..."
        features = YAML.load_file("cache/ge0_features_#{ARGV[0]}_#{i}")
      else
        # construct two Group object
        g1 = Group.new(c.articles.last)
        c.articles.each do |a|
          g1.articles << a
        end
        g1.articles.pop
        # contsturct g.articles of all other concepts
        other_c = conceptSet.clone
        other_c.delete(c)
        g2 = Group.new(other_c.last.articles.last)
        other_c.each do |c|
          c.articles.each do |a|
            g2.articles << a
          end
        end
        g2.articles.pop


        # get feature of this two catogroy
        features = get_feature [i,0,g1,g2]
        f = File.new("cache/ge0_features_#{ARGV[0]}_#{i}","w")
        f.puts features.to_yaml
        f.close
      end

      correct = -1
      while correct < 0.9
        correct = -1
        f_sel = ''
        del_f = ''
        min_dis = Float::INFINITY
        
        print "features.size: ",features.size
        conceptSet_end = nil
        features.each do |f_hash|
          f = f_hash[0]
          print "\nfeature: ",f

          articleSet_c = []
          if File.exist? "cache/ge0_features_#{ARGV[0]}_#{i}_#{f}"
            print "\nload cache cache/ge0_features_#{ARGV[0]}_#{i}_#{f}"
            articleSet_c = YAML.load_file("cache/ge0_features_#{ARGV[0]}_#{i}_#{f}")
          else
            print "all_articles.size: ",all_articles.size,"\n"
            all_articles.each_with_index do |a,ind|
              if ind%5000 == 0 then print ind," " end
              a_vector = YAML.load_file("articles_vector/#{a.id}")
              if a_vector.has_key? f
                print a_vector[f]," "
                a.tags[f] = a_vector[f]
              else
                a.tags[f] = 0.0
              end
              articleSet_c << a
            end
            print "write out cache ge0_features_#{ARGV[0]}_#{i}_#{f}"
            file = File.new("cache/ge0_features_#{ARGV[0]}_#{i}_#{f}","w")
            file.puts articleSet_c.to_yaml
            file.close
          end

          print "\nclustering..."
          print "\narticleSet_c.size: ",articleSet_c.size
          conceptSet_new = clustering($CLUSERTING_TYPE, articleSet_c,"_m2_#{i}_#{f}")
          # compare conceptSet
          print "\nconceptSet distance..."
          intersect = distance(conceptSet_new,conceptSet_ori)
          # binding.pry
          intersect /= all_articles.size

          if intersect > correct
            correct = intersect
            print "\ncorrect: ",correct
            f_sel = f
            del_f = f_hash
            conceptSet_end = conceptSet_new.clone
          end
        end
        puts "\ndone sel feature"
        puts "f_sel: ",f_sel
        # select feature
        features_sel << f_sel
        features.delete(del_f)
        if conceptSet_end.nil?
          binding.pry
        end
        conceptSet = conceptSet_end.clone
        all_articles.each do |a|
          a_vector = YAML.load_file("articles_vector/#{a.id}")
          # icfSet[a.id][f]
          if a_vector.has_key? f_sel
            print "!"
            a.tags[f_sel] = a_vector[f_sel]
          else
            a.tags[f_sel] = 0
          end
        end

        print "\ncorrect: ",correct,"\n"
        if features.empty?
          print "\nfeatures.empty!"
          break
        end
      end
      print "write out cache features_sel_2c#{i} and all_articles_2c#{i}..."
      f = File.new("cache/features_sel_9/features_sel_2c#{i}","w")
      f.puts features_sel.to_yaml
      f.close
      f = File.new("cache/features_sel_9/all_articles_2c#{i}","w")
      f.puts all_articles.to_yaml
      f.close
      f = File.new("cache/features_sel_9/conceptSet_2c#{i}","w")
      f.puts conceptSet.to_yaml
      f.close
      
    end
  end

  f=File.new("cache/generation0_fel_#{ARGV[0]}","w")
  f.puts features_sel.to_yaml
  f.close
end




