load 'node.rb'
require 'yaml'

conceptSet = YAML.load_file("cache/clustering_3falsefalsefalse")
userSet = YAML.load_file("userSet")
print "conceptSet.size: ",conceptSet.size,"\n"
# conceptSet.each do |c|
#   # print c.number_of_users.to_f," "
#   # print "c.articles.size: ",c.articles.size,"\n"
#   c.articles.each do |a|
#     printf "|"
#     print a.users.size," " 
#   end
# end
print "userSet.size: ",userSet.size,"\n"
conceptSet.each_with_index do |c, index|
  # if index%10 == 0 then print "." end
  print "."
  # if this user see any articles in this concept,
  users = []
  c.articles.each do |a|
    # printf "|"
    users = a.users | users
  end
  users.uniq!
  c.number_of_users = users.size
end
print "c.number_of_users: "
conceptSet.each_with_index do |c, index|
  print c.number_of_users," "
end